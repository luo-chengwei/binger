#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

__author__ = 'Chengwei Luo (luo.chengwei@gatech.edu)'
__version__ = '0.1.2'
__date__ = 'December 2014'

"""
BinGeR (Binner for Genome Recovery): 
	in-situ Genome recovery tool for metagenomes

Copyright(c) 2014 Chengwei Luo (luo.chengwei@gatech.edu)

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/BinGeR

This script is part of the BinGeR package,

for help for BinGeR main function, type:
python BinGeR.py --help

for help for BinGeR.preprocessing function, type:
python BinGeR.preprocessing.py --help
"""

USAGE = \
"""Usage: %prog <required_parameters> [options]

BinGeR: in-situ Genome recovery tool for metagenomes

BinGeR (Binner for Genome Recovery) is a platform for de novo genome recovery 
from series metagenomes. It integrates information from single copy gene content, 
contig coverage correlation, and contig oligo-nucleotide composition correlation, 
and contig alignments to bin genome fragments together. 
It is written in Python, therefore it should run on Mac OS, and Unix/Linux. 

Add --help to see a full list of required and optional
arguments to run BinGeR.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/BinGeR/wiki

If you use BinGeR in your work, please cite it as:
<BinGeR citation here>

Copyright: Chengwei Luo, Konstantinidis Lab, Georgia Institute of Technology, 2013
"""


import sys
import os
import re
import glob
from optparse import OptionParser, OptionGroup
from subprocess import call, PIPE, Popen
from time import ctime, time
from datetime import timedelta
import cPickle
import string
import multiprocessing as mp
from itertools import product
from operator import itemgetter

import numpy as np
from scipy import stats
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import networkx as nx

########################## GLOBAL VARS #################
transtab = string.maketrans('ATGC', 'TACG')
base={'A':'00', 'N':'00', 'C':'01', 'G':'10', 'T':'11'}
num_entry = 1167708

############################### CLASSES ###################################
class ORF:
    '''ORF class'''
    def __init__(self, fa, type='DNA'):
        self.fa = fa
        self.type = type
        if self.type == 'DNA':
			self.startcodon = 'ATG'
			self.stopcodons = ['TAG', 'TGA', 'TAA']
        else:
			self.startcodon = 'AUG'
			self.stopcodons = ['UAG', 'UGA', 'UAA']
        self.plus_orfs = []
        self.minus_orfs = []
        self.orf_number = 0

    def oneStrandORF(self, reverse_flag, index, length):
        """input a SeqRecord object
        reverse_flag: true or false, true means reverse it,
        index: start from 0 when plus, add plus when reverse"""
        recs = []
        loclis = []
        startpos = 0
        index = int(index)
        seq = self.fa.seq
        if reverse_flag:
            seq = Seq.reverse_complement(seq)
        while seq.count(self.startcodon,startpos) > 0:
            pos = seq.find(self.startcodon,startpos)
            loclis.append(pos)
            startpos = pos + len(self.startcodon)
        i = 1
        pend_d = {} #check children orfs belong to another long orf -- aviod this
        mend_d = {}
        for loc in loclis:
            alreadyexsit = True
            stop_loc = len(seq)
            for codon_loc in range(loc, len(seq), 3):
            	codon = seq[codon_loc:codon_loc+3]
            	if str(codon).upper() in self.stopcodons:
            		stop_loc = codon_loc + 3
            		break
            nt_seq = seq[loc:stop_loc]
            
            if len(nt_seq) >= int(length):
                nt_id = self.fa.id + '|orf_'  + str(i+index)
                if stop_loc == None: stop_loc = len(seq)
                if reverse_flag:
                    nt_id += '|' + str(len(seq)-stop_loc+1) + '-' +str(len(seq)-loc) + '|-'
                    mend = len(seq)-stop_loc+1
                    if mend not in mend_d:
                        mend_d[mend] = 1
                        alreadyexsit = False
                else:
                    nt_id += '|' + str(loc+1) + '-' + str(stop_loc) + '|+'
                    if stop_loc not in pend_d:
                        pend_d[stop_loc] = 1
                        alreadyexsit = False
                nt_rec = SeqRecord(nt_seq, id=nt_id, description='')
                if not alreadyexsit:
                    recs.append(nt_rec)
                    i += 1
        return recs
        
    def getORFs(self,length):
        self.plus_orfs = self.oneStrandORF(False,0,length)
        self.minus_orfs = self.oneStrandORF(True,len(self.plus_orfs),length)
        self.orf_number = len(self.plus_orfs) + len(self.minus_orfs)

class mappingDetails:
	def __init__(self, qlength, tlength, alength, identity, bitscore):
		self.query_length = qlength
		self.target_length = tlength
		self.align_length = alength
		self.identity = identity
		self.bitscore = bitscore

class taxonTree:
	def __init__(self):
		self.tree = nx.DiGraph()
		self.name2taxonID = {}
		self.taxonID2name = {}
	
	def __init__(self, dir):
		self.tree = nx.DiGraph()
		try:
			nameslib = glob.glob(dir + '/names.dmp')[0]
		except:
			sys.stderr.write('FATAL: cannot find the names.dmp file, initilization aborted.\n')
			exit(1)
		
		try:
			nodeslib = glob.glob(dir + '/nodes.dmp')[0]
		except:
			sys.stderr.write('FATAL: cannot find the nodes.dmp file, initilization aborted.\n')
			exit(1)
		
		# construct name -> taxonID mapping	
		self.name2taxonID = {}
		self.taxonID2name = {}
		for line in open(nameslib, 'r'):
			cols = line.split('\t')
			taxonID = cols[0]
			name = cols[2]
			self.name2taxonID[name] = taxonID
			if cols[-2] == 'scientific name':
				self.taxonID2name[taxonID] = name
			
		# construct node tree
		edges = []
		nodes = {}
		for line in open(nodeslib, 'r'):
			cols = line.split('\t')
			parentNode = cols[2]
			childrenNode = cols[0]
			rank = cols[4]
			nodes[childrenNode] =rank
			if childrenNode != parentNode:
				edges.append((childrenNode, parentNode))
		
		self.tree.add_edges_from(edges)
		nx.set_node_attributes(self.tree, 'rank', nodes)
	
	def getTaxonPath(self, taxonID):
		p = [taxonID]
		currentNode = taxonID
		if currentNode not in self.tree.nodes():
			return []
		while len(self.tree.successors(currentNode)) > 0:
			p.append(self.tree.successors(currentNode)[0])
			currentNode = self.tree.successors(currentNode)[0]
	
		path = []
		for taxonID in p:
			try: name = self.taxonID2name[taxonID]
			except KeyError: name = 'NA'
			rank = self.tree.node[taxonID]['rank']
			path.append((taxonID, name, rank))
			
		return path
	
class BinGeR_Project:
	def __init__(self):
		# info
		self.samples = []
		self.min_length = 0
		self.kmer = 0
		self.cov_coeff = 0
		self.kmer_coeff = 0
		self.read_type = None
		# data 
		self.sequences = {}
		self.internal_IDs = None
		self.phylo_tags = None
		self.coverage = None
		self.kmer_zscores = None
	
	def load_project(self, info, dir):
		# loading info
		self.samples = info.samples
		self.min_length = info.min_length
		self.kmer = info.kmer
		self.read_type = info.read_type
		
		# loading data
		try:
			for record in SeqIO.parse(dir+'/all_contigs.fa', 'fasta'):
				self.sequences[record.name] = record.seq
		except:
			sys.stderr.write('[FATAL] Error in loading assembly sequences.\n')
			exit(1)
		try:
			self.internal_IDs = cPickle.load(open(dir+'internal_IDs.cPickle', 'rb'))
		except:
			sys.stderr.write('[FATAL] Error in loading internal IDs.\n')
			exit(1)
		try:
			self.phylo_tags = cPickle.load(open(dir+'phylo.tags.cPickle', 'rb'))
		except:
			sys.stderr.write('[FATAL] Error in loading phylogenetic tags.\n')
			exit(1)
		try:
			self.coverage = cPickle.load(open(dir+'coverage.cPickle','rb'))
		except:
			sys.stderr.write('[FATAL] Error in loading coverage profiles.\n')
			exit(1)
		try:
			self.kmer_zscores = cPickle.load(open(dir+'all_contigs.kmer.cPickle','rb'))
		except:
			sys.stderr.write('[FATAL] Error in loading K-mer Z-scores.\n')
			exit(1)
		return 0
		
class ProjectInfo:
	def __init__(self):
		self.samples = []
		self.reads_dir = None
		self.assembly = None
		self.outfile = None
		self.DBs = []
		self.tempdir = None
		self.min_length = 0
		self.kmer = 0
		self.quiet = False
		self.num_proc = 0
		self.blat = None
		self.bowtie2 = None
		self.bowtie2_build = None
		self.samtools = None
		self.read_type = None
		
	def initProject(self, options):
		# Reads the sample list and check if every required file is in place
		try:
			sfh = open(options.sample_list, 'r')
		except:
			sys.stderr.write("FATAL: Cannot open supplied sample list: %s\n" % options.sample_list)
			exit(1)
			
		while 1:
			sample = sfh.readline().rstrip('\n')
			if not sample:
				break
			sample = sample.replace(' ', '', sample.count(' '))
			if sample != '':
				self.samples.append(sample)
		sfh.close()
		
		if len(self.samples) == 1:
			sys.stderr.write("\n**************************************************************\n")
			sys.stderr.write("[WARNING]: BinGeR is optimized for serial metagenomes, you\n")
			sys.stderr.write("           supplied only one sample. BinGeR will continue the\n")
			sys.stderr.write("           binning, but the results might not be as reliable.\n")
			sys.stderr.write("**************************************************************\n\n")
		
		# check co-assembly
		if os.path.exists(options.assembly):
			self.assembly = os.path.abspath(options.assembly)
		else:
			sys.stderr.write('[FATAL]: failed to locate co-assembly file: %s\n' % options.assembly)
			exit(1)
		
		# Check all the read files
		sys.stdout.write("Checking if every read file in place...\n")
		self.reads_dir = os.path.abspath(options.reads_dir)
		for sample in self.samples:
			readFiles = self.getReadsFile(sample, options.read_type)
			if len(readFiles) == 0:
				sys.stderr.write('[FATAL]: failed to locate reads for sample: %s' % sample)
				exit(1)
		# check if db/ has every file needed.
		self.check_db(options)
		sys.stdout.write("Done. Everything looks fine.\n")
		
		# initialized a tempdir for temp files
		if options.tempdir != None:
			if os.path.exists(options.tempdir):
				self.tempdir = os.path.abspath(options.tempdir)
			else:
				sys.stderr.write("FATAL: cannot locate temp directory you supplied: %s\n" % options.tempdir)
				exit(1)
		elif options.cont == True:
			dirs = glob.glob(os.getcwd() + '/BinGeR_temp_*')
			if len(dirs) != 1:
				sys.stderr.write("FATAL: failure in resolving last run's temp directory!\n")
				exit(1)
			else: self.tempdir = dirs[0]
		else:		
			import tempfile
			try:
				self.tempdir = tempfile.mkdtemp(prefix='BinGeR_temp_', dir = os.getcwd())
				with open(self.tempdir + '/tasks.log', 'w') as tfh: tfh.write('0\n')
		
			except OSError:
				sys.stderr.write("FATAL: cannot create temporary output directory, please make sure you have proper privilege!\n")
				exit(1)
		
		# add all the options to ProjectInfo obj
		self.min_length = options.min_length
		self.kmer = options.kmer
		self.quiet = options.quiet
		self.blat = options.blat
		self.bowtie2 = options.bowtie2
		self.bowtie2_build = options.bowtie2_build
		self.samtools = options.samtools
		self.read_type = options.read_type
		self.num_proc = options.num_proc
		self.outfile = options.outfile
	
		return 0
		
	def printInfo(self):
		sys.stdout.write("Co-assembly: %s\n" % self.assembly)
		sys.stdout.write("\nSamples used in this BinGeR run:\n\n")
		for sample in self.samples:
			sys.stdout.write("  Sample: %s\n"%sample)
			read_files, type, format = self.getReadsFile(sample, self.read_type)
			sys.stdout.write("  %s reads in %s format:\n" % (type.upper(), format))
			for file in read_files:
				sys.stdout.write('    %s\n' % file)
			sys.stdout.write('\n')
		
		sys.stdout.write("Temporary directory used: %s \n" % self.tempdir)
		sys.stdout.write("Number of CPUs used: %i \n" % self.num_proc)
		sys.stdout.write("Output preprocessed database: %s\n" % self.outfile)
		sys.stdout.write("Parameters used in this run:\n")
		sys.stdout.write("K-mer length: %i\n" % self.kmer)
		sys.stdout.write("Minimum length for contigs to be considered: %i\n" % self.min_length)
		sys.stdout.write("\n")
		return 0
	
	def getReadsFile(self, sample, type):
		## We leave options open for future dev to accept SE reads, but altering the return flag
		if type == 'interleaved':
			readsFiles = glob.glob(self.reads_dir+'/'+sample+'.[12].f*')
			if len(readsFiles) == 0:
				sys.stderr.write("FATAL: fail to locate interleaved reads for sample:%s\n"%sample)
				exit(1)
			elif len(ReadsFiles) > 1:
				sys.stderr.write("FATAL: ambiguity in locating reads for sample:%s\n"%sample)
				exit(1)
			
			file_format = 'fastq'
			with open(readsFiles[0], 'r') as rfh:
				line = rfh.readline().rstrip('\n')
				if line[0] == '>':
					file_format = 'fasta'
				elif line[0] == '@':
					pass
				else:
					sys.stderr.write("FATAL: read file doesn't look like in FastA/Q format:%s\n"%sample)
					exit(1)
			
			return ((readsFiles[0]), 'interleaved', file_format)
				
		elif type == 'pe':
			readsFiles_1 = glob.glob(self.reads_dir+'/'+sample+'.1.f*')
			if len(readsFiles_1) == 0:
				sys.stderr.write("FATAL: fail to locate forward reads for sample:%s\n"%sample)
				exit(1)
			elif len(readsFiles_1) > 1:
				sys.stderr.write("FATAL: ambiguity in locating forward reads for sample:%s\n"%sample)
				exit(1)
			
			file_format = 'fastq'
			with open(readsFiles_1[0], 'r') as rfh:
				line = rfh.readline().rstrip('\n')
				if line[0] == '>':
					file_format = 'fasta'
				elif line[0] == '@':
					pass
				else:
					sys.stderr.write("FATAL: read file doesn't look like in FastA/Q format:%s\n"%sample)
					exit(1)
			
			readsFiles_2 = glob.glob(self.reads_dir+'/'+sample+'.2.f*')
			if len(readsFiles_2) == 0:
				sys.stderr.write("FATAL: fail to locate reverse reads for sample:%s\n"%sample)
				exit(1)
			elif len(readsFiles_2) > 1:
				sys.stderr.write("FATAL: ambiguity in locating reverse reads for sample:%s\n"%sample)
				exit(1)
			
			file_format = 'fastq'
			with open(readsFiles_2[0], 'r') as rfh:
				line = rfh.readline().rstrip('\n')
				if line[0] == '>':
					file_format = 'fasta'
				elif line[0] == '@':
					pass
				else:
					sys.stderr.write("FATAL: read file doesn't look like in FastA/Q format:%s\n"%sample)
					exit(1)
			
			return ((readsFiles_1[0], readsFiles_2[0]), 'pe', file_format)
		
		elif type == 'se':
			readsFiles = glob.glob(self.reads_dir+'/'+sample+'.f*')
			if len(readsFiles) == 0:
				sys.stderr.write("FATAL: fail to locate SE reads for sample:%s\n"%sample)
				exit(1)
			elif len(readsFiles_1) > 1:
				sys.stderr.write("FATAL: ambiguity in locating SE reads for sample:%s\n"%sample)
				exit(1)
			
			file_format = 'fastq'
			with open(readsFiles[0], 'r') as rfh:
				line = rfh.readline().rstrip('\n')
				if line[0] == '>':
					file_format = 'fasta'
				elif line[0] == '@':
					pass
				else:
					sys.stderr.write("FATAL: read file doesn't look like in FastA/Q format:%s\n"%sample)
					exit(1)
					
			return ((readsFiles[0]), 'se', file_format)
			
		else:
			sys.stderr.write("FATAL: --reads_type should be \"pe\", \"se\", or \"interleaved\", you supplied: %s\n" % type)
			exit(1)
			
	def check_db(self, options):
		sys.stdout.write("Checking db files...\n")
		# shallow search with local package	
		db_dir = os.path.abspath(sys.argv[0]).replace('BinGeR.preprocessing.py', 'db/')
		dbs = ["phylo.tar.gz", "taxonomy.tar.gz"]
		flag = 0
		for filename in dbs:
			if not os.path.exists(db_dir + filename):
				sys.stderr.write('FATAL: Error in initializing database file: %s\n' % filename)
				exit(1)
			self.DBs.append(db_dir + filename)
		return 0
	
		
# end of class ProjectInfo

################################ UTILITY FUNCTIONS ################################
def depositImage(projInfo):
	projImage_file = projInfo.tempdir + '/proj.info'
	try: 
		cPickle.dump(projInfo, open(projImage_file, 'wb'))
	except:
		sys.stderr.write('[FATAL]: Failure in creating project image.\n')
		exit(1)
		
def loadImage(tempdir):
	projImage_file = tempdir + '/proj.info'
	if not os.path.exists(projImage_file):
		sys.stderr.write('[FATAL]: Failure in loading project information.\n')
		exit(1)
	else:
		return cPickle.load(open(projImage_file, 'rb'))

def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

############################# FILTERING CONTIGS #############################
def filter_and_ID_map(projInfo):
	progress_file = projInfo.tempdir + '/tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 1:
		return 0
	quiet = projInfo.quiet
	num_proc = projInfo.num_proc
	pre_dir = projInfo.tempdir
	contig_file = pre_dir + '/all_contigs.fa'
	mapping_file = pre_dir + '/internal_IDs.cPickle'
	
	if not quiet:
		sys.stdout.write('  Now filtering contigs and building internal contig IDs dictionary...\n')
	ofh = open(contig_file, 'w')
	mapping = {}
	assembly = projInfo.assembly
	for index, record in enumerate(SeqIO.parse(assembly, 'fasta')):
		L = len(record.seq)
		if L < projInfo.min_length: continue
		tag = str(index+1)+'.'+str(L)
		mapping[tag] = record.description
		ofh.write('>%s\n%s\n' % (tag, record.seq))
	ofh.close()
	cPickle.dump(mapping, open(mapping_file, 'wb'))
	
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('1\n')
	
	if not quiet:
		sys.stdout.write('  Done.\n')

############################# KMER PART #############################
def rc(seq):
	return str(seq)[::-1].translate(transtab)

def cseq(seq):
	return str(seq).translate(transtab)
	
def kmer_zscore(seq, K, kmerIndex, N):
	kmers = np.ones(N)
	seq = seq.upper()
	for i in range(len(seq) - K + 1):
		kmer = seq[i:i+4]
		kmers[kmerIndex[kmer]] += 1
		rv_kmer = rc(kmer)
		kmers[kmerIndex[rv_kmer]] += 1
	return stats.zscore(kmers)
	
def cal_kmer(args):
	contigs, kmer_pickle, K, min_length = args
	kmerIndex = {}
	# count palindromic kmers as the same, e.g., GATC and CTAG should be the same
	for i, a in enumerate(list(product('ACGT', repeat=K))):
		kmer = ''.join(a)
		if kmer in kmerIndex: continue
		if kmer != rc(kmer):
			kmerIndex[kmer] = i
			kmerIndex[rc(kmer)] = i
		else:
			kmerIndex[kmer] = i
	
	# re-index kmerIndex
	uniqIndex = {}
	index = 0
	for j in kmerIndex.values():
		if j not in uniqIndex:
			uniqIndex[j] = index
			index += 1	
	for kmer in kmerIndex:
		x = kmerIndex[kmer]
		index = uniqIndex[x]
		kmerIndex[kmer] = index

	N = len(uniqIndex)  # N is the dimension
	
	kmer_zscores = {}
	for record in SeqIO.parse(contigs, 'fasta'):
		tag = record.description
		seq = str(record.seq).upper()
		seq = seq.replace('N','A', seq.count('N'))
		L = len(seq)
		if L < min_length: continue
		elif L <= 10000:   # if length <= 10Kbp, direct count it
			xtag = tag+'|1-'+str(L)
			kmer_zscores[xtag] = kmer_zscore(seq, K, kmerIndex, N)
		else:  # when length > 10kbp, split it into 10kbp chunks
			for i in range(0, L, 10000):
				j = i+10000
				if L - j < 10000 and j < L: j = L
				elif j > L: continue
				subseq = seq[i:j]
#				if len(subseq) < 10000: print i, j, L
				xtag = tag+'|'+str(i+1)+'-'+str(j)
				kmer_zscores[xtag] = kmer_zscore(subseq, K, kmerIndex, N)	
		
	# serialize results
	cPickle.dump(kmer_zscores, open(kmer_pickle, 'wb'))
	
	return 0
		
	
def kmer_zscores(projInfo):
	progress_file = projInfo.tempdir + '/tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 2:
		return 0
	quiet = projInfo.quiet
	kmer = projInfo.kmer
	min_length = projInfo.min_length
	pre_dir = projInfo.tempdir+'/'
	num_proc = projInfo.num_proc
	kmer_pickle = pre_dir + 'all_contigs.kmer.cPickle'
	infile = pre_dir + 'all_contigs.fa'
	
	if not quiet:
		sys.stdout.write('  Now calculating Z-score of %i-mer for contigs > %i bp...\n' % (kmer, min_length))
	
	if num_proc == 1:
		cal_kmer([infile, kmer_pickle, kmer, min_length])
	elif num_proc > 1:
		# split the input contig fasta file into subfiles
		ofhs = []
		for i in range(num_proc):
			ofhs.append(open(infile+'.'+str(i+1), 'w'))
		for i, record in enumerate(SeqIO.parse(infile, 'fasta')):
			ofh_index = i % num_proc
			SeqIO.write(record, ofhs[ofh_index], 'fasta')
		for ofh in ofhs: ofh.close()
		
		cmds = []
		for i in range(num_proc):
			input_fa = infile+'.'+str(i+1)
			output_pickle = infile.replace('.fa', '.kmer.'+str(i+1)+'.cPickle')
			if os.path.exists(output_pickle): continue
			cmds.append([input_fa, output_pickle, kmer, min_length])
		
		pool = mp.Pool(num_proc)
		pool.map_async(cal_kmer, cmds)
		pool.close()
		pool.join()
		
		zscores = {}
		for i in range(num_proc):
			subfile = pre_dir + 'all_contigs.kmer.' + str(i+1) + '.cPickle'
			zscores.update(cPickle.load(open(subfile, 'rb')))
			os.unlink(subfile)
		cPickle.dump(zscores, open(kmer_pickle, 'wb'))
	
	# clean up
	for i in range(1, num_proc+1):
		subfa = pre_dir + 'all_contigs.fa.' + str(i)
		try:
			os.unlink(subfa)
		except OSError:
			pass
		
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('2\n')
	
	if not quiet:
		sys.stdout.write('  Done.\n')
	
	return 0
	
############################# COVERAGE MAPPING #############################
def subsample_reads(x):
	readFile_res, prefix, max_num = x
	read_files, type, file_format = readFile_res
	if len(read_files) == 1:
		read_file = read_files[0]
	elif len(read_files) == 2:
		read_file_1, read_file_2 = read_files
	else:
		sys.stderr.write('[FATAL]: Error in subsampling reads file: %s\n' % sample)
	
	n = 0
	if type == 'se':
		if file_format == 'fastq': ofh = open(prefix + '.fq', 'w')
		else: ofh = open(prefix + '.fa', 'w')
		ifh = open(read_file, 'r')
		while n < max_num:
			n += 1
			tag = ifh.readline().rstrip('\n')
			if not tag: break
			seq = ifh.readline().rstrip('\n')
			if file_format == 'fastq':
				ofh.write('@%s\n%s\n' % (tag[1:], seq))
				for i in range(2): 
					line = ifh.readline().rstrip('\n')
					ofh.write('%s\n' % line)
			else:
				ofh.write('>%s\n%s\n' % (tag[1:], seq))
		ifh.close()
		ofh.close()
		
	elif type == 'interleaved':
		ifh = open(read_file, 'r')
		if file_format == 'fastq': 
			ofh1 = open(prefix + '.1.fq', 'w')
			ofh2 = open(prefix + '.2.fq', 'w')
		else: 
			ofh1 = open(prefix + '.1.fa', 'w')
			ofh2 = open(prefix + '.1.fa', 'w')
		while n < max_num:
			n += 2
			tag1 = ifh.readline().rstrip('\n')
			if not tag: break
			seq1 = ifh.readline().rstrip('\n')
			if file_format == 'fastq':
				ofh1.write('@%s\n%s\n' % (tag1[1:], seq1))
				for i in range(2): 
					line = ifh.readline().rstrip('\n')
					ofh1.write('%s\n' % line)
			else:
				ofh1.write('>%s\n%s\n' % (tag1[1:], seq1))
			tag2 = ifh.readline().rstrip('\n')
			if not tag: break
			seq2 = ifh.readline().rstrip('\n')
			if file_format == 'fastq':
				ofh2.write('@%s\n%s\n' % (tag2[1:], seq2))
				for i in range(2): 
					line = ifh.readline().rstrip('\n')
					ofh2.write('%s\n' % line)
			else:
				ofh2.write('>%s\n%s\n' % (tag2[1:], seq2))
		ifh.close()
		ofh1.close()
		ofh2.close()
			
	else:
		ifh1 = open(read_file_1, 'r')
		ifh2 = open(read_file_2, 'r')
		if file_format == 'fastq': 
			ofh1 = open(prefix + '.1.fq', 'w')
			ofh2 = open(prefix + '.2.fq', 'w')
		else: 
			ofh1 = open(prefix + '.1.fa', 'w')
			ofh2 = open(prefix + '.1.fa', 'w')
		while n < max_num:
			n += 2
			tag1 = ifh1.readline().rstrip('\n')
			if not tag1: break
			seq1 = ifh1.readline().rstrip('\n')
			if file_format == 'fastq':
				ofh1.write('@%s\n%s\n' % (tag1[1:], seq1))
				for i in range(2):
					line = ifh1.readline().rstrip('\n')
					ofh1.write('%s\n' % line)
			else:
				ofh1.write('>%s\n%s\n' % (tag1[1:], seq1))
				
			tag2 = ifh2.readline().rstrip('\n')
			seq2 = ifh2.readline().rstrip('\n')
			if file_format == 'fastq':
				ofh2.write('@%s\n%s\n' % (tag2[1:], seq2))
				for i in range(2):
					line = ifh2.readline().rstrip('\n')
					ofh2.write('%s\n' % line)
			else:
				ofh2.write('>%s\n%s\n' % (tag2, seq2))
			
		ifh1.close()
		ifh2.close()
		ofh1.close()
		ofh2.close()

def runBowtie2(x):
	samtools, bowtie2, pre_dir, sample, bowtie2db, bowtie2_out, bowtie2_log = x
	reads = glob.glob(pre_dir + '/' + sample + '.*.f[aq]')
	if len(reads) == 1: file_type = 'single'
	elif len(reads) == 2: file_type = 'mated'
	if reads[0].split('.')[-1] == 'fa': file_format = 'fasta'
	else: file_format = 'fastq'
	lfh = open(bowtie2_log, 'w')
	
	if file_type == 'single':
		if file_format == 'fastq':
			bowtie2_cmd = [bowtie2, '-x', bowtie2db, '-U', reads[0]]
		else:
			bowtie2_cmd = [bowtie2, '-f', '-x', bowtie2db, '-U', reads[0],]
	else:
		if file_format == 'fastq':
			bowtie2_cmd = [bowtie2, '-x', bowtie2db, '-1', reads[0], '-2', reads[1]]
		else:
			bowtie2_cmd = [bowtie2, '-f', '-x', bowtie2db, '-1', reads[0], '-2', reads[1]]
	
	samtools_view = [samtools, 'view', '-bhS', '-']
	samtools_sort = [samtools, 'sort', '-', re.sub('.bam$', '', bowtie2_out)]
	samtools_index = [samtools, 'index', bowtie2_out]
	
	p1 = Popen(bowtie2_cmd, stdout = PIPE, stderr = lfh)
	p2 = Popen(samtools_view, stdin = p1.stdout, stdout = PIPE, stderr = lfh)
	p3 = Popen(samtools_sort, stdin = p2.stdout, stdout = PIPE, stderr = lfh)
	p1.stdout.close()
	p2.stdout.close()
	output, err = p3.communicate()
	call(samtools_index, stderr = lfh, stdout = lfh)
	lfh.close()
		
	
def mapping_reads(projInfo):
	progress_file = projInfo.tempdir + '/tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 3:
		return 0
		
	quiet = projInfo.quiet
	pre_dir = projInfo.tempdir+'/'
	num_proc = projInfo.num_proc
	bowtie2_build = projInfo.bowtie2_build
	bowtie2 = projInfo.bowtie2
	samtools = projInfo.samtools
	contig_file = pre_dir + 'all_contigs.fa'
	
	if not quiet:
		sys.stdout.write('  Now mapping reads to contigs...\n')
	
	# check which samples need to be calculated
	samples_to_cal = {}
	for sample in projInfo.samples:
		coverage_pickle = pre_dir + '/' + sample + '.coverage.cPickle'
		if os.path.exists(coverage_pickle): continue
		samples_to_cal[sample] = coverage_pickle
		
	if len(samples_to_cal) < 1:
		if not options.quiet:
			sys.stdout.write('  BAM files are ready.\n')
		return 0
	
	if not quiet:
		sys.stdout.write('  Now mapping reads to contigs by Bowtie2...\n')
	
	bowtie2db = pre_dir + '/all_contigs'
	db_flag = 0
	for ext in ['1.bt2', '2.bt2', '3.bt2', '4.bt2', 'rev.1.bt2', 'rev.2.bt2']:
		if not os.path.exists(pre_dir + '/all_contigs.'+ext):
			db_flag = 1
			break
	if not quiet:
		sys.stdout.write('    Bowtie2 building reference index...\n')
	if db_flag == 1:
		call([bowtie2_build, contig_file, bowtie2db], stdout = PIPE, stderr = PIPE)
	if not quiet:
		sys.stdout.write('    Done.\n')
	
	# start bowtie2 mapping
	if not quiet:
		sys.stdout.write('    Bowtie2 mapping...\n')
		
	bowtie2_cmds = []
	subsample_reads_cmds = []
	for sample in samples_to_cal:
		bowtie2_out = pre_dir + '/' + sample + '.bam'
		bowtie2_log = pre_dir + '/' + sample + '.bowtie2.log'
		input_files = glob.glob(pre_dir + '/' + sample + '.reads.*')
		read_prefix = pre_dir + '/' + sample + '.reads'
		if os.path.exists(bowtie2_out):
			continue
		readFile_res = projInfo.getReadsFile(sample, projInfo.read_type)
		if len(input_files) == 0:
			subsample_reads_cmds.append([readFile_res, read_prefix, 4e7])
		bowtie2_cmds.append([samtools, bowtie2, pre_dir, sample, bowtie2db, bowtie2_out, bowtie2_log])
	
	if len(subsample_reads_cmds) > 0:	
		if not quiet:
			sys.stdout.write('      Subsampling reads...\n')
		
		pool = mp.Pool(num_proc)
		pool.map_async(subsample_reads, subsample_reads_cmds)
		pool.close()
		pool.join()
		
		if not quiet:
			sys.stdout.write('      Done.\n')
	
	if len(bowtie2_cmds) > 0:	
		if not quiet:
			sys.stdout.write('      Now Bowtie2 mapping...\n')		

		pool = mp.Pool(min(num_proc, bowtie2_cmds))
		pool.map_async(runBowtie2, bowtie2_cmds)
		pool.close()
		pool.join()	

		if not quiet:
			sys.stdout.write('      Mapping finished.\n')
	
	# clean up
	for fq in glob.glob(projInfo.tempdir + '/*.fq'): os.unlink(fq)
	for log in glob.glob(projInfo.tempdir + '/*.log'): os.unlink(log)
	for bt2 in glob.glob(projInfo.tempdir + '/*.bt2'): os.unlink(bt2)
	
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('3\n')
	
	if not quiet:
		sys.stdout.write('  Done.\n')
	return 0

############################# COVERAGE CALCULATION #############################
def base_index(base):
	if base == 'A' or base == 'a': return 0
	if base == 'C' or base == 'c': return 1
	if base == 'G' or base == 'g': return 2
	if base == 'T' or base == 't': return 3
	return 0

def interpret_pstring(ref_base, x):
	res = {'start':0, 'end':0, 'insert':0, 'del':0, 'bases': [0,0,0,0]}
	if x == '': return res
	
	res['end'] = x.count('$')
	x = re.sub(r'\$', '', x)
	res['start'] = x.count('^')
	x = re.sub(r'\^.{1}','',x)
	
	for a in re.findall(r'\+\d+', x):
		res['insert'] += 1
		c = int(a.replace('+', ''))
		x=re.sub(r'\+\d+\w{%i}' % c, '', x)
	for a in re.findall(r'\-\d+', x):
		res['del'] += 1
		c = int(a.replace('-', ''))
		x = re.sub(r'\-\d+\w{%i}' % c, '', x)
		
	res['bases'][base_index(ref_base)] += (x.count(',') + x.count('.'))
	res['bases'][0] += x.count('A') + x.count('a')
	res['bases'][1] += x.count('C') + x.count('c')
	res['bases'][2] += x.count('G') + x.count('g')
	res['bases'][3] += x.count('T') + x.count('t')
	
	return res
	
	
def bam2mpileup(args):
	samtools, contigs_file, bamfile, mpileup_file, mpileup_log = args
	mfh = open(mpileup_file, 'w')
	lfh = open(mpileup_log, 'w')
	mpileup_cmd = [samtools, 'mpileup', '-C50', '-d10000', '-Df', contigs_file, bamfile]
	call(mpileup_cmd, stdout = mfh, stderr = lfh)
	mfh.close()
	lfh.close()
	

def mpileup2cov(args):
	sample, mpileup_file, contigs_file, subpickle = args
	cov = {sample:{}}
	
	# build up dict of all ref contigs
	X = {}
	for record in SeqIO.parse(contigs_file, 'fasta'):
		L = int(record.name.split('.')[1])
		X[record.name] = []
		cov[sample][record.name] = []
		for i in range(L): X[record.name].append(0)
		
	# go through mpileup file to calculate the coverage
	for line in open(mpileup_file, 'r'):
		contig, pos_st, ref_base, cov_st, pileup_st, qual_st = line[:-1].split('\t')
		pos = int(pos_st) - 1
		X[contig][pos] = int(cov_st)
	
	for contig in cov[sample]:
		L = int(contig.split('.')[1])
		for start_index in range(0, L, 100):
			seg = len(X[contig][start_index:start_index+100])
			if seg <= 0: continue
			cov[sample][contig].append(float(sum(X[contig][start_index:start_index+100]))/seg)
			
	cPickle.dump(cov, open(subpickle, 'wb'))
	return 0
		
def bam2coverage(projInfo):
	progress_file = projInfo.tempdir + '/tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 4:
		return 0
		
	quiet = projInfo.quiet
	pre_dir = projInfo.tempdir+'/'
	num_proc = projInfo.num_proc
	contigs_file = pre_dir + 'all_contigs.fa'
	
	if not quiet:
		sys.stdout.write('  Converting BAMs to coverage profiles via mpileup...\n')
	
	mpileup_files = []
	bamfiles = []
	mpileup_cmds = []
	for sample in projInfo.samples:
		bamfile = pre_dir + sample + '.bam'
		bamfiles.append(bamfile)
		mpileup_file = pre_dir +  sample + '.mpileup'
		mpileup_log = pre_dir + sample + '.mpileup.log'
		mpileup_files.append(mpileup_file)
		if os.path.exists(mpileup_file): continue
		mpileup_cmds.append([projInfo.samtools, contigs_file, bamfile, mpileup_file, mpileup_log])
	# multiprocessing pool to mpileup reads	
	if len(mpileup_cmds) > 0:
		pool = mp.Pool(min(num_proc, len(projInfo.samples)))
		pool.map_async(bam2mpileup, mpileup_cmds)
		pool.close()
		pool.join()		
		if not quiet:
			sys.stdout.write('  Conversion finished.\n')
	else:
		if not quiet:
			sys.stdout.write('  No conversion needed.\n')
	
	if not quiet:
		sys.stdout.write('  Converting mpileup files to coverage...\n')
	mpileup2cov_cmds = []
	for sample in projInfo.samples:
		mpileup_file  = pre_dir + sample + '.mpileup'
		subpickle = pre_dir + sample + '.cov.cPickle'
		if os.path.exists(subpickle): continue
		mpileup2cov_cmds.append([sample, mpileup_file, contigs_file, subpickle])
	
	if len(mpileup2cov_cmds) > 0:
		# multiprocessing pool to mpileup reads
		pool = mp.Pool(min(num_proc, len(mpileup2cov_cmds)))
		pool.map_async(mpileup2cov, mpileup2cov_cmds)
		pool.close()
		pool.join()
		if not quiet:
			sys.stdout.write('  Conversion finished.\n')
	else:
		if not quiet:
			sys.stdout.write('  No conversion needed.\n')
	
	X = {}		
	coverage_pickle = pre_dir + 'coverage.cPickle'
	if not quiet:
		sys.stdout.write('  Packaging coverage information...\n')
	for sample in projInfo.samples:
		if not quiet: sys.stdout.write('    coverage information for sample: %s.\n' % sample)
		subpickle = pre_dir + sample + '.cov.cPickle'
		if not os.path.exists(subpickle):
			sys.stderr.write('[FATAL]: Failure in locating coverage information for sample: %s. Exiting.\n' % sample)
			exit(1)
		X.update(cPickle.load(open(subpickle, 'rb')))
	# transform coverage data	
	coverage = {}
	ext_sample = projInfo.samples[0]
	for contig in X[ext_sample]: coverage[contig] = []
	for contig in coverage:
		for sample in projInfo.samples:
			coverage[contig].append(X[sample][contig])
		
	# cleanup
	X = {}
	for sample in projInfo.samples:
		subpickle = pre_dir + sample + '.cov.cPickle'
		mpileup_file = pre_dir + sample + '.mpileup'
		bamfile = pre_dir + sample + '.bam'
		baifile = pre_dir + sample + '.bam.bai'
		mplog_file = mpileup_file + '.log'
		os.unlink(subpickle)
		os.unlink(mpileup_file)
		os.unlink(mplog_file)
		os.unlink(bamfile)
		os.unlink(baifile)
		
	if not quiet: sys.stdout.write('  Done. Serializing bulk coverage information...\n')
	cPickle.dump(coverage, open(coverage_pickle, 'wb'))	
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('4\n')
	if not quiet:
		sys.stdout.write('  Done.\n')
	return 0

		
############################# ORFS #############################
def run_orf(x):
	infile, outfile = x
	informat = 'fasta'
	outformat = 'fasta'
	ofh = open(outfile, 'w')
	for record in SeqIO.parse(infile, informat):
		orfobj = ORF(record, type='DNA')
		orfobj.getORFs(500)  # limit the ORF lower length cutoff at 500bp
		for rec in orfobj.plus_orfs:
			SeqIO.write(rec, ofh, outformat)
		for rec in orfobj.minus_orfs:
			SeqIO.write(rec, ofh, outformat)
	ofh.close()

def extract_ORFs(projInfo):
	pre_dir = projInfo.tempdir + '/'
	progress_file = pre_dir + 'tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 5:
		return 0
	outfile = projInfo.tempdir + '/all_contigs.ORFs.fa'
	num_proc = projInfo.num_proc
	infile = projInfo.tempdir + '/all_contigs.fa'
	quiet = projInfo.quiet
	
	if num_proc > 1:
		ofhs = []
		for i in range(num_proc):
			ofhs.append(open(infile+'.'+str(i+1), 'w'))
		for i, record in enumerate(SeqIO.parse(infile, 'fasta')):
			ofh_index = i % num_proc
			SeqIO.write(record, ofhs[ofh_index], 'fasta')
		for ofh in ofhs: ofh.close()
	
	if not quiet:
		sys.stdout.write('  Now extracting ORFs from filtered contigs...\n')
		
		cmds = []
		for i in range(num_proc):
			input_fa = infile+'.'+str(i+1)
			output_orf = outfile + '.' + str(i+1)
			cmds.append([input_fa, output_orf])
		
		pool = mp.Pool(num_proc)
		pool.map_async(run_orf, cmds)
		pool.close()
		pool.join()
			
		ofh = open(outfile, 'w')
		for i in range(num_proc):
			subfile = outfile + '.' + str(i+1)
			for line in open(subfile, 'r'):
				ofh.write(line)
			os.unlink(subfile)
		ofh.close()
	else:
		run_orf([infile, outfile])
	
	# clean up
	for i in range(1, num_proc+1):
		subfa = pre_dir + 'all_contigs.fa.' + str(i)
		try:
			os.unlink(subfa)
		except OSError:
			pass
	
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('5\n')
	
	if not quiet:
		sys.stdout.write('  Done.\n')
	
	return 0

############################# PHYLO TAGS #############################
def runBLAT(x):
	blat, db, infile, outfile, index, n, q = x
	if not q:
		sys.stdout.write('    BLAT search merged assembly ORFs against sub-phylo_db [%i/%i].\n'%(index+1, n))
	cmd = [blat, db, infile, '-out=blast8', outfile]
	call(cmd, stdout = PIPE, stderr = PIPE)
	
def search_phylo(projInfo):
	progress_file = projInfo.tempdir + '/tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 6:
		return 0
		
	pre_dir = projInfo.tempdir
	sub_db_dir = pre_dir + '/db/'
	blat = projInfo.blat
	num_proc = projInfo.num_proc
	quiet = projInfo.quiet
	if not quiet:
		sys.stdout.write('  Now assigning phylo tags to assembled contigs...\n')
	seed_sorted_file = pre_dir + '/all_contigs.ORFs.vs.seeds.sorted.blat'
	seed_file = pre_dir + '/all_contigs.ORFs.vs.seeds.blat'
	uniq_file = pre_dir + '/all_contigs.ORFs.vs.mpa.sorted.bt2'
	orf_fa = pre_dir + '/all_contigs.ORFs.fa'
	
	# seed blat search
	if not os.path.exists(seed_sorted_file):
		# prepare DBs
		if not os.path.exists(sub_db_dir): os.mkdir(sub_db_dir)
		if not quiet:
			sys.stdout.write('    Initializing reference database now...\n')
		seed_db = projInfo.DBs[0]
		if not os.path.exists(seed_db): seed_db = os.path.dirname(sys.argv[0])+'/db/phylo.tar.gz'
		if not os.path.exists(seed_db):
			sys.stderr.write('[FATAL]: Cannot locate phylo DB.\n')
			exit(1)
		cmd = ['tar', '-xzvf', seed_db, '-C', sub_db_dir]
		if not quiet:
			sys.stdout.write('    Decompressing databases...\n')
		call(cmd, stderr = PIPE, stdout = PIPE)
		if not quiet:
			sys.stdout.write('    Done.\n')
	
		if not quiet:	
			sys.stdout.write('    Splitting database...\n')
		temp_fa = sub_db_dir+'/seeds.db'
		chunk_size = 2*(int(1+float(num_entry)/num_proc))
		cmd = ['split', '-l', str(chunk_size), temp_fa, temp_fa+'.']
		call(cmd, stdout = PIPE, stderr = sys.stderr)
		os.unlink(temp_fa)
		if not quiet:
			sys.stdout.write('    Done.\n')
	
		ori_dbs = sorted(glob.glob(sub_db_dir+'seeds.db.*'))
		for x, dbFile in enumerate(ori_dbs):
			infile = re.sub('db.\w{2}$', str(x+1)+'.db', dbFile)
			call(['mv', dbFile, infile])
			
		DBs = glob.glob(sub_db_dir+'*.db')
		n_db = len(DBs)
		if not os.path.exists(seed_file):
			cmds = []
			for i, db in enumerate(DBs):
				outfile = pre_dir + '/all_contigs.ORFs.vs.seeds.' + str(i+1) + '.blat'
				if os.path.exists(outfile): continue
				cmds.append([blat, db, orf_fa, outfile, i, n_db, quiet])
			
			pool = mp.Pool(num_proc)
			pool.map_async(runBLAT, cmds)
			pool.close()
			pool.join()
		
			if not quiet:
				sys.stdout.write('    Done.\n')
				
			ofh = open(seed_file, 'w')
			for i in range(n_db):
				seed_subfile = pre_dir + '/all_contigs.ORFs.vs.seeds.' + str(i+1) + '.blat'
				for line in open(seed_subfile, 'r'):
					cols = line[:-1].split('\t')
					identity, align_length, evalue = float(cols[2]), int(cols[3]), float(cols[-2])
					if identity < 70 or align_length < 100 or evalue > 1e-6: continue
					ofh.write(line)
				os.unlink(seed_subfile)
			ofh.close()
			
			if not quiet:
				sys.stdout.write('    Sorting BLAT output...')
			cmd = ['sort', seed_file, '-o', seed_sorted_file]
			call(cmd, stdout = PIPE, stderr = PIPE)
			if not quiet:
				sys.stdout.write('Done!\n')
			os.unlink(seed_file)
	
	if os.path.exists(sub_db_dir):
		import shutil
		shutil.rmtree(sub_db_dir)
	
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('6\n')
	
	if not quiet:
		sys.stdout.write('  Done.')

	return 0			

def phylo_tag(projInfo):
	progress_file = projInfo.tempdir + '/tasks.log'
	bit = int(open(progress_file, 'r').readline()[:-1])
	if bit >= 7:
		return 0
	from operator import itemgetter
		
	pre_dir = projInfo.tempdir
	seed_sorted_file = pre_dir + '/all_contigs.ORFs.vs.seeds.sorted.blat'
	if not os.path.exists(seed_sorted_file):
		sys.stderr.write('[FATAL]: cannot locate the BLAT file for seeds search.\n')
		exit(1)
	phylo_pickle = pre_dir + '/phylo.tags.cPickle'
	num_proc = projInfo.num_proc
	quiet = projInfo.quiet
	
	taxDB = projInfo.DBs[1]
	if not os.path.exists(taxDB): taxDB = os.path.dirname(sys.argv[0])+'/db/taxonomy.tar.gz'
	if not os.path.exists(taxDB):
		sys.stderr.write('[FATAL]: Cannot locate taxonomy DB.\n')
		exit(1)
	db_dir = pre_dir + 'db/'
	if not os.path.exists(db_dir): os.mkdir(db_dir)
	cmd = ['tar', '-xzvf', taxDB, '-C', db_dir]
	call(cmd, stderr = PIPE, stdout = PIPE)
	
	# init taxonomy tree
	if not quiet: sys.stdout.write('  Now initiating taxonomy tree...\n')
	tTree = taxonTree(db_dir)
	if not quiet: sys.stdout.write('  Done.\n')
	
	if not quiet:
		sys.stdout.write('  Now recording phylogenetic information of ORFs...\n')
	
	phylo_tags = {}
	taxIDs = {}
	for line in open(seed_sorted_file, 'r'):
		cols = line[:-1].split('\t')
		orf, target, identity, align_length, evalue, bitscore = \
				cols[0], cols[1], float(cols[2]), int(cols[3]), float(cols[-2]), float(cols[-1])
		contigID, orfID, orf_coor, orf_strand = orf.split('|')
		orf_start, orf_end = orf_coor.split('-')
		orf_length = int(orf_end) - int(orf_start) + 1
		taxID, nucl_gi, genome_coor, anno = target.split('.')
		gene_start, gene_end = re.search('(\d+)\-(\d+)', genome_coor).group(1,2)
		gene_length = abs(int(gene_end) - int(gene_start)) + 1
		if contigID not in phylo_tags: phylo_tags[contigID] = {}
		if orfID not in phylo_tags[contigID]: phylo_tags[contigID][orfID] = []
		# adding entries
		mapping_details = mappingDetails(orf_length, gene_length, align_length, identity, bitscore)
		phylo_tags[contigID][orfID].append([bitscore, mapping_details, taxID, anno])
	if not quiet:
		sys.stdout.write('  Done reading BLAT file. Now processing the results...\n')
	
	# sort entries, leave top 5 bitscore hits
	for contigID in phylo_tags:
		for orfID in phylo_tags[contigID]:
			sorted_results = sorted(phylo_tags[contigID][orfID], key = lambda x: x[0], reverse = True)[:5]
			for bitscore, mapping_details, taxID, anno in sorted_results:
				taxIDs[taxID] = []
			phylo_tags[contigID][orfID] = sorted_results
			
	# adding taxonomy paths
	for taxID in taxIDs: taxIDs[taxID] = tTree.getTaxonPath(taxID)
	for contigID in phylo_tags:
		for orfID in phylo_tags[contigID]:
			for hit_index in range(len(phylo_tags[contigID][orfID])):
				taxID = phylo_tags[contigID][orfID][hit_index][2]
				taxPath = taxIDs[taxID]
				phylo_tags[contigID][orfID][hit_index].append(taxPath)
		
	cPickle.dump(phylo_tags, open(phylo_pickle, 'wb'))	
	os.unlink(seed_sorted_file)
	if os.path.exists(db_dir):
		import shutil
		shutil.rmtree(db_dir)
		
	with open(projInfo.tempdir + '/tasks.log', 'w') as tfh: tfh.write('7\n')
	if not quiet: sys.stdout.write('  Done.\n')

	return 0

############################# INTEGRATE DATA #############################
def integrate_lib(projInfo, options):
	quiet = projInfo.quiet
	pre_dir = projInfo.tempdir+'/'
	if options.outfile == None:
		outfile = projInfo.outfile
	else:
		outfile = os.path.abspath(options.outfile)
	
	if not quiet:
		sys.stdout.write('  Now integrate all resulting into final database...\n')
	
	P = BinGeR_Project()  # BinGeR_Project obj init
	if not quiet:
		sys.stdout.write('    Loading all data into project object...\n')
	P.load_project(projInfo, pre_dir) # load all data onto project
	if not quiet:
		sys.stdout.write('    Done.\n')
	if not quiet:
		sys.stdout.write('    Now serializing database...\n')
	
	cPickle.dump(P, open(outfile, 'wb'))
	
	if not quiet:
		sys.stdout.write('  All finished. BinGeR preprocessed database saved at: %s\n' % outfile)
	return 0

################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + __version__)
	
	# Required arguments
	requiredOptions = OptionGroup(parser, "Required options",
								"These options are required to run BinGeR.preprocessing, and may be supplied in any order.")
	
	requiredOptions.add_option("-l", "--sample_list", type = "string", metavar = "FILE",
							help = "Text file containing all sample names, one per line")

	requiredOptions.add_option("-r", "--reads_dir", type = "string", metavar = "DIR",
							help = "Directory where reads in fastq or fasta format are, \
							naming follows \"sample.*.1.fq\" and \"sample.*.2.fq\" convention \
							(or \"sample.*.1.fa\" and \"sample.*.2.fa\" for fasta files).")
	
	requiredOptions.add_option("-a", "--assembly", type = "string", metavar = "FILE",
							help = "The co-assembly contigs in fasta format.")
	
	requiredOptions.add_option("-o", "--outfile", type = "string", metavar = "FILE", 
							help = "The output library storing all the preprocessing data.")
							
	parser.add_option_group(requiredOptions)

	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"These options are optional, and may be supplied in any order.")

	optOptions.add_option("--read_type", type = "string", default = "pe", metavar = "STRING",
							help = "How reads are stored: [pe]: the i-th forward end is stored at *.1.fq and its reverse mate is the i-th entry in *.2.fq; [interleaved]:\
							assuming the i-th read is the forward end and the (i+1)-th read is the mate; [se]: unpaired single end reads. [Default: pe]")

	optOptions.add_option("--tempdir", type = "string", metavar = "DIR",
							help = "If re-run a previously failed task, you can use this option to point to previous temp dir if there were ambiguities in\
							locating tempdir; this automatically sets --continue option to be True.")
	
	parser.add_option_group(optOptions)	
	
	# preprocessing parameters that could fine tune the process
	preOptions = OptionGroup(parser, "Preprocessing parameters",
						"These options are optional, and may be supplied in any order.")
	
	preOptions.add_option("--min_length", type = "int", default = 500, metavar = 'INT',
							help = "Minimum contig length to be considered in binning [default: 500].")
	
	preOptions.add_option("-k", "--kmer", type = "int", default = 4, metavar = 'INT',
							help = "K-mer size for oligo-nucleotide frequency statistics [default: K = 4; range: 3-8].")
						
	parser.add_option_group(preOptions)
	
	# runtime settings that could affect the performance and messaging
	runtimeSettings = OptionGroup(parser, "Runtime settings",
						"These options are optional, and may be supplied in any order.")
	
	runtimeSettings.add_option("-t", "--num_proc", type = "int", default = 1, metavar = 'INT',
							help = "Number of processor for BinGeR to use [default: 1].")
				
	runtimeSettings.add_option("-q", "--quiet", default = False, action = "store_true",
								help = "Suppress printing detailed runtime information, only important messages will show [default: False].")

	runtimeSettings.add_option("--keep", default = False, action = "store_true",
								help = "Keeps the folder that the intermediary files are stored at [default: False].")

	runtimeSettings.add_option("--continue", default = False, action = "store_true", dest = "cont",
								help = "Continue from last run if it failed in the process. [default: False].")

		
	parser.add_option_group(runtimeSettings)
	
	# 3rd party applications required by BinGeR preprocessing
	extAppOptions = OptionGroup(parser, "3rd party application parameters",
						"These options are optional, and may be supplied in any order. If these binaries are not found in PATH, \
						you can explicitly specify each of their locations by \"--blat\", \"--bowtie2\", and \"--bowtie2_build\",\
						 you can also just specify the Bowtie2 package directory by using \"--bowtie2_dir\". ")

	extAppOptions.add_option("--bowtie2_dir", type = "string", metavar = 'DIR',
							help = "Path to bowtie2 bin directory, specify if bowtie2 and bowtie2-build not in env path.")
	
	extAppOptions.add_option("--bowtie2", type = "string", default = "bowtie2",metavar = 'STRING',
							help = "Path to bowtie2, specify if bowtie2 not in env path. [default: bowtie2]")
	
	extAppOptions.add_option("--bowtie2_build", type = "string", default = "bowtie2-build",metavar = 'STRING',
							help = "Path to bowtie2-build, specify if bowtie2-build not in env path. [default: bowtie2-build]")
	
	extAppOptions.add_option("--samtools", type = "string", default = "samtools", metavar = 'STRING',
							help = "Path to SAMtools, specify if SAMtools not in env path. [default: samtools]")
	
	extAppOptions.add_option("--blat", type = "string", default = "blat", metavar = 'STRING',
							help = "Path to BLAT, specify if BLAT not in env path. [default: blat]")
	
	parser.add_option_group(extAppOptions)
	
	(options, args) = parser.parse_args(argv)
	
	if not options.cont:
		# check all the required parameters	
		if options.sample_list is None:
			parser.error("A list of samples and a working directory are required!")
			exit(1)
	
		if options.reads_dir is None:
			parser.error("A directory containing sample reads are required!")
			exit(1)
	
		if options.assembly is None:
			parser.error("A co-assembly fasta file is required!")
			exit(1)
	
		if options.outfile is None:
			parser.error("An output database for preprocessing is required!")
			exit(1)
	
		# check all the optional parameters	
		if options.kmer < 3 or options.kmer > 8:
			parser.error("Kmer should be an integer ranging between 3 and 8, you supplied %i" % options.kmer)
			exit(1)
	
		if options.num_proc < 1:
			parser.error("Number of CPUs has to be positive integer, you supplied: %i" % options.num_proc)
			exit(1)
	
		# check if all required 3rd party programs are in place.
		if which('bowtie2') == None or which('bowtie2-build') == None:
			if options.bowtie2_dir == None:
				sys.stderr.write('[FATAL]: cannot locate Bowtie2.\n')
				exit(1)
			else:
				options.bowtie2 = os.path.abspath(options.bowtie2_dir) + '/bowtie2'
				options.bowtie2_build = os.path.abspath(options.bowtie2_dir) + '/bowtie2-build'
				if which(options.bowtie2) == None:
					sys.stderr.write('[FATAL]: cannot locate bowtie2 in the Bowtie2 binaries path you supplies: %s.\n' % options.bowtie2_dir)
					exit(1)
				if which(options.bowtie2_build) == None:
					sys.stderr.write('[FATAL]: cannot locate bowtie2-build in the Bowtie2 binaries path you supplies: %s.\n' % options.bowtie2_dir)
					exit(1)
	
		if which('samtools') == None or which(options.samtools) == None:
			sys.stderr.write('[FATAL]: cannot locate SAMtools in PATH or the SAMtools binary you supplied: %s.\n' % options.samtools)
			exit(1)
	
		if which('blat') == None or which(options.blat) == None:
			sys.stderr.write('[FATAL]: cannot locate BLAT in PATH or the BLAT binary you supplied: %s.\n' % options.blat)
			exit(1)
	
		# check sanity of the files in required directories
		projInfo = ProjectInfo()
		projInfo.initProject(options)
		depositImage(projInfo)
		if not options.quiet:
			projInfo.printInfo()	
	else:
		projInfo = loadImage(options.tempdir)
		projInfo.tempdir = options.tempdir

	######################## START PREPROCESSING #####################
	total_start_time = time()
	sys.stdout.write("BinGeR preprocessing started at %s\n"%(ctime()))
	sys.stdout.flush()
	
	# preprocessing 
	filter_and_ID_map(projInfo)  # 1, filter contigs by length and build up internal IDs
	kmer_zscores(projInfo)   # 2, get Z-score of contigs
	mapping_reads(projInfo)  # 3, map reads to ref contigs
	bam2coverage(projInfo) # 4, get coverage for contigs
	extract_ORFs(projInfo)    # 5, get ORFs of co-assembly
	search_phylo(projInfo)  # 6, blat search ORFs against phylophlan seed genes
	phylo_tag(projInfo)   # 7, add phylo tags to ORFs
	integrate_lib(projInfo, options) # 8, integrate all data into one single project library
	
	
	if not options.keep:
		try:
			import shutil
			shutil.rmtree(projInfo.tempdir)
		except OSError:
			pass
	else:
		sys.stdout.write('  Preprocessing temporary files stored at: %s.\n\n' % os.path.abspath(projInfo.tempdir))
		
	total_start_time = time()
	sys.stdout.write("BinGeR preprocessing finished at %s\n"%(ctime()))
	sys.stdout.flush()
		
	return 0
	
# End of main
	
if __name__ == '__main__':
	main()
	# End of main
