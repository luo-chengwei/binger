#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

__author__ = 'Chengwei Luo (luo.chengwei@gatech.edu)'
__version__ = '0.1.2'
__date__ = 'December 2014'

"""
BinGeR (Binner for Genome Recovery): 
	in-situ Genome recovery tool for metagenomes

Copyright(c) 2014 Chengwei Luo (luo.chengwei@gatech.edu)

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/BinGeR

for help, type:
python BinGeR.py --help
"""

USAGE = \
"""Usage: %prog <required_parameters> [options]

BinGeR: in-situ Genome recovery tool for metagenomes

BinGeR (Binner for Genome Recovery) is a platform for de novo genome recovery 
from series metagenomes. It integrates information from single copy gene content, 
contig coverage correlation, and contig oligo-nucleotide composition correlation, 
and contig alignments to bin genome fragments together. 
It is written in Python, therefore it should run on Mac OS, and Unix/Linux. 

Add --help to see a full list of required and optional
arguments to run BinGeR.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/BinGeR/wiki

If you use BinGeR in your work, please cite it as:
<BinGeR citation here>

Copyright: Chengwei Luo, Konstantinidis Lab, Georgia Institute of Technology, 2013
"""


import sys
import os
import re
import glob
import cPickle
from optparse import OptionParser, OptionGroup
from subprocess import call, PIPE
from time import ctime, time
from datetime import timedelta
from operator import itemgetter

import networkx as nx
from itertools import combinations
from scipy.spatial import distance
import numpy as np
import scipy.cluster.hierarchy as hierarchy
from cyflann import *
import fastcluster

from community import *
#from commPageRank import commPageRank as commpr

################################## GLOBALS ################################
SC_GENES = ['rpsF', 'aspS', 'uvrB', 'gmk', 'tsf', 'rnc', 'rpsJ', 'ychF', 'leuS', 
			'cgtA', 'grpE', 'dnaX', 'ftsY', 'tyrS', 'rplX', 'rplV', 'rplW', 'rplT', 
			'ffh', 'proS', 'rplS', 'rplP', 'rplQ', 'rplN', 'rplO', 'rplL', 'rplM', 
			'rplJ', 'rplK', 'ksgA', 'hisS', 'rplF', 'secE', 'rplD', 'rplE', 'rplB', 
			'rplC', 'rplA', 'ligA', 'prfA', 'rpoC', 'rpoB', 'rpoA', 'cysS', 'recA', 
			'pheS', 'pheT', 'valS', 'rpsG', 'glyS', 'rpsE', 'rpsD', 'rpsC', 'rpsB', 
			'rpsO', 'rpsM', 'rpsL', 'rpsK', 'pgk', 'rpsI', 'rpsH', 'coaE', 'rpsT', 
			'rpsS', 'rpsR', 'rpsQ', 'rpsP', 'thrS', 'mraW', 'fmt', 'frr', 'smpB', 
			'rplU', 'serS', 'rplR', 'infC', 'infB', 'rpmI', 'rpmH', 'rpmA', 'rpmC', 
			'rpmB', 'rpmF', 'nusG', 'nusA', 'dnaA', 'alaS', 'era', 'dnaK', 'lepA', 
			'dnaN', 'argS', 'mnmA', 'rplI', 'tilS', 'rfbA', 'nag', 'ybeY', 'engA', 
			'tig', 'secA', 'gyrB', 'secG', 'gyrA', 'ileS', 'pyrG', 'secY']


################################## CLASSES ################################
class mappingDetails:
	def __init__(self, qlength, tlength, alength, identity, bitscore):
		self.query_length = qlength
		self.target_length = tlength
		self.align_length = alength
		self.identity = identity
		self.bitscore = bitscore
	def align_perc(self):
		return float(self.align_length)/min(self.query_length, self.target_length)

class BinGeR_Project:
	def __init__(self):
		# info
		self.samples = []
		self.min_length = 0
		self.kmer = 0
		self.read_type = None
		
		# DATA SECTION
		# internal_IDs is a dict keyed by internal IDs and valued by corresponding external contig IDs
		# the internal IDs look like: [contig index].[contig length]
		# for instance, 1233.4359 would be the 1233-th contig with 4359bp in length
		self.internal_IDs = None
		
		# phylo_tags is a dict of dicts keyed by contigID (internal) and then by orf_ID
		# and valued by a tuple of info:
		# below is the detail: 
		# phylo_tags[contigID][orfID].append([bitscore, mapping_details, taxID, anno, taxDetails])
		# in which the "mapping_details" is a mappingDetails obj.
		self.phylo_tags = None
		
		# coverage is a dict, keyed by contigIDs and valued by a list of lists
		# lists arranged in samples order, in each sub-list, it's the per 100-base coverage
		self.coverage = None
		
		# kmer_zscores is a dict of numpy arrays. dict is keyed by xtags: 
		# xtag = [contig tag]|[start loc]-[end loc]; 
		# this is to mitigate the effect of chimeric contigs
		# the values are numpy arrays, which are Z-scores of the corresponding sequences
		self.kmer_zscores = None
	
	def coverage_clustering(self, cov_data, contigIDs, options):
		import warnings
		quiet = options.quiet
		num_proc = options.num_proc
		chunk_size = options.chunk_size
		initGraphPickle = options.outdir + '/initGraph.cPickle'
		if os.path.exists(initGraphPickle):
			return None
		
		# make a cov_dict
		cov_dict = {}
		for c, contigID in zip(cov_data, contigIDs):
			cov_dict[contigID] = c	
		self.cov_dict = cov_dict
			
		# take them into chunks
		cov_blocks = list(chunks(cov_data,chunk_size))
		contigID_blocks = list(chunks(contigIDs, chunk_size))
		blocks = zip(contigID_blocks, cov_blocks)
		
		# rough fast clustering over sparse contig space
		if not quiet:
			sys.stdout.write("      Fast hierarchical clustering on coverage correlation...\n")
		flatClusterIDs = []
		for contigID_block, cov_block in blocks:
			X = []
			D = []
			for contigID, cov in zip(contigID_block, cov_block):
				X.append(cov)
				D.append(contigID)
			fclust_linkage = fastcluster.linkage(X, method = 'complete', metric = 'correlation', preserve_input = True)
			np.clip(fclust_linkage, 0., 1e10, fclust_linkage)
			flat_cluster = hierarchy.fcluster(fclust_linkage, .3, criterion = 'distance')
			fclust_IDs = []
			for x in range(max(flat_cluster)): fclust_IDs.append([])
			for clusterID, label in zip(flat_cluster, D):
				fclust_IDs[clusterID-1].append(label)
			# merge onto the same cluster
			for x in fclust_IDs: flatClusterIDs.append(x)
		
		# further clustering, finer resolution
		centroids = []
		for x in flatClusterIDs:
			centroids.append(getCentroid(cov_dict, x))
		
		centroidX = []
		for centroidID in centroids:
			centroidX.append(cov_dict[centroidID])	
		fclust_linkage = fastcluster.linkage(centroidX, method = 'complete', metric = 'correlation', preserve_input = True)
		np.clip(fclust_linkage, 0., 1e10, fclust_linkage)
		flat_cluster = hierarchy.fcluster(fclust_linkage, .3, criterion = 'distance')
		
		centroid_clusters = []
		for i in range(max(flat_cluster)): centroid_clusters.append([])
		for clusterID, centroidIndex in zip(flat_cluster, range(len(centroids))):
			centroid_clusters[clusterID-1].append(centroidIndex)
						
		fclust_labels = []
		fclust_covs = []
		for centroid_cluster in centroid_clusters:
			fclust_labels.append([])
			fclust_covs.append([])
			for clusterIndex in centroid_cluster:
				for contigID in flatClusterIDs[clusterIndex]:
					fclust_labels[-1].append(contigID)
					fclust_covs[-1].append(cov_dict[contigID])
			
		if not quiet:
			sys.stdout.write("      Done.\n")

		# cluster coverage vectors
		G = nx.Graph()
		G.add_nodes_from(contigIDs)
		
		if not quiet:
			sys.stdout.write("      Calculating normalized Manhattan distance and Pearson corr. coeff. matrices in %i blocks...\n" % len(fclust_labels))
		
		if len(self.samples) > 1:
			for block_index, (block_cov, block_label) in enumerate(zip(fclust_covs, fclust_labels)):
				if not quiet:
					sys.stdout.write('      Block [%i/%i] %i contigs\n' % (block_index+1, len(fclust_covs), len(block_label)))
				block_corrcoef = 1-distance.cdist(block_cov, block_cov, metric = 'correlation')
				block_cov_dist = distance.cdist(block_cov, block_cov, metric = 'cityblock')
				qualified_coors = np.argwhere(block_corrcoef > 0.8)
				for i, j in qualified_coors:
					labelA = block_label[i]
					labelB = block_label[j]
					if labelA == labelB: continue
					coeff = block_corrcoef[i, j]
					norm_manhattan = block_cov_dist[i, j]/min(sum(cov_dict[labelA]), sum(cov_dict[labelB]))
					if norm_manhattan < 0.8 and norm_manhattan > 0.2: continue
					G.add_edge(labelA, labelB, corr_coeff = coeff, norm_manhattan = norm_manhattan)
					
		else:# N == 1 trivial case, can only use cov_dist
			for block_index, (block_cov, block_label) in enumerate(zip(fclust_covs, fclust_labels)):
				if not quiet:
					sys.stdout.write('      Block [%i/%i] %i contigs\n' % (block_index+1, len(fclust_covs), len(block_label)))
				block_cov_dist = distance.cdist(block_cov, block_cov, metric = 'cityblock')
				for i in range(len(block_label)):
					for j in range(i+1, len(block_label)):
						labelA = block_label[i]
						labelB = block_label[j]
						if labelA == labelB: continue
						norm_manhattan = cov_dist[i, j]/min(sum(cov_dict[labels[i]]), sum(cov_dict[labels[j]]))
						if norm_manhattan < 0.8 and norm_manhattan > 0.2: continue
						G.add_edge(labelA, labelB, corr_coeff = 1., norm_manhattan = norm_manhattan)
		if not quiet:
			sys.stdout.write("      Done.\n")

		for contigID in cov_dict:
			cov_vector = cov_dict[contigID]
			G.node[contigID]['coverage'] = cov_vector
		
		return G
		
	def ZScore_clustering(self, options):
		from itertools import combinations
		quiet = options.quiet
		num_proc = options.num_proc
		K = self.kmer
		zscores = self.kmer_zscores
		outdir = options.outdir
		initGraphPickle = options.outdir + '/initGraph.cPickle'
		if os.path.exists(initGraphPickle):
			return None
		
		# prepare data
		# code the phylo-labeled set as anchors
		if not quiet:
			sys.stdout.write('      %i sequence fragments to cluster.\n' % 	len(zscores))

		tSet = []
		labels = []
		tag_dict = {}
		label_index = 0
		for xtag in zscores:
			contig, seg_tag = xtag.split('|')
			if contig not in tag_dict: tag_dict[contig] = []
			tag_dict[contig].append((seg_tag, label_index))
			label_index += 1
			tSet.append(zscores[xtag])
			labels.append(xtag)
		
		# take them into chunks
		tSet_blocks = list(chunks(tSet, options.chunk_size))
		xtag_blocks = list(chunks(labels, options.chunk_size))
		blocks = zip(xtag_blocks, tSet_blocks)
		
		# rough fast clustering over sparse contig space
		if not quiet:
			sys.stdout.write("      Fast hierarchical clustering on kmer correlation...\n")
		flatClusterIDs = []
		for xtag_block, tSet_block in blocks:
			X = []
			D = []
			for xtag, kmer_freq in zip(xtag_block, tSet_block):
				X.append(kmer_freq)
				D.append(xtag)
			fclust_linkage = fastcluster.linkage(X, method = 'complete', metric = 'correlation', preserve_input = True)
			np.clip(fclust_linkage, 0., 1e10, fclust_linkage)
			flat_cluster = hierarchy.fcluster(fclust_linkage, .3, criterion = 'distance')
			fclust_IDs = []
			for x in range(max(flat_cluster)): fclust_IDs.append([])
			for clusterID, label in zip(flat_cluster, D):
				fclust_IDs[clusterID-1].append(label)
			# merge onto the same cluster
			for x in fclust_IDs: flatClusterIDs.append(x)
		
		# further clustering, finer resolution
		centroids = []
		for x in flatClusterIDs:
			centroids.append(getCentroid(self.kmer_zscores, x))
		centroidX = []
		for centroidID in centroids:
			centroidX.append(self.kmer_zscores[centroidID])	
		fclust_linkage = fastcluster.linkage(centroidX, method = 'complete', metric = 'correlation', preserve_input = True)
		np.clip(fclust_linkage, 0., 1e10, fclust_linkage)
		flat_cluster = hierarchy.fcluster(fclust_linkage, .3, criterion = 'distance')
		
		centroid_clusters = []
		for i in range(max(flat_cluster)): centroid_clusters.append([])
		for clusterID, centroidIndex in zip(flat_cluster, range(len(centroids))):
			centroid_clusters[clusterID-1].append(centroidIndex)
		
		fclust_labels = []
		fclust_kmers = []
		for centroid_cluster in centroid_clusters:
			fclust_labels.append([])
			fclust_kmers.append([])
			for clusterIndex in centroid_cluster:
				for xtag in flatClusterIDs[clusterIndex]:
					fclust_labels[-1].append(xtag)
					fclust_kmers[-1].append(self.kmer_zscores[xtag])
		if not quiet:
			sys.stdout.write("      Done.\n")
				
		# create vectors
		G = nx.Graph()
		G.add_nodes_from(tag_dict.keys())
		if not quiet:
			sys.stdout.write('      Creating K-mer clustering graph in blocks...\n')
		for block_index, (block_kmer, block_label) in enumerate(zip(fclust_kmers, fclust_labels)):
			if not quiet:
				sys.stdout.write('      Block [%i/%i] %i contigs\n' % (block_index+1, len(fclust_kmers), len(block_label)))		
			kmer_coeffs = 1 - distance.cdist(block_kmer, block_kmer, metric = 'correlation')
			qualified_coors = np.argwhere(kmer_coeffs > 0.8)
			for i, j in qualified_coors:
				tagA = block_label[i]
				tagB = block_label[j]
				contigA = tagA.split('|')[0]
				contigB = tagB.split('|')[0]
				if contigA == contigB: continue
				if contigA not in G[contigB]:
					G.add_edge(contigA, contigB, kmer_coeff = [kmer_coeffs[i,j]])
				else:
					G[contigA][contigB]['kmer_coeff'].append(kmer_coeffs[i,j])
							
		if not quiet:
			sys.stdout.write('      K-mer graph construction completed.\n')
		
		return G

	def init_cores(self, options):
		finalCores_pickle =  options.outdir+'/finalCores.cPickle'
		if os.path.exists(finalCores_pickle):
			return None, None
		
		from itertools import combinations
		quiet = options.quiet
		
		core_pickle =  options.outdir+'/initCores.cPickle'
		graph_pickle = options.outdir+'/initGraph.cPickle'
		
		if os.path.exists(graph_pickle):
			if not quiet:
				sys.stdout.write('      Loading initial integrated graph from %s\n' % graph_pickle)
			initGraph = cPickle.load(open(graph_pickle, 'rb'))
			if not quiet:
				sys.stdout.write('      Done.\n')
		else:
			if not quiet:
				sys.stdout.write('      Initiating integrated graph...\n')
			
			# integrate coverage and kmer graphs
			initGraph = nx.Graph()
			initGraph.add_nodes_from(self.coverage.keys())
			
			# get contigs indexed, sorted by length, prep clustering data
			contigs = self.coverage.keys()
			contigIDs = sorted(contigs, key = lambda x: int(x.split('.')[1]), reverse = True)
			contigID_dict = {}
			cov_data = []
			for contigIndex, contigID in enumerate(contigIDs):
				contigID_dict[contigID] = contigIndex
				normed_cov = []
				for cov in self.coverage[contigID]:
					nomial_mean = sum(cov)/len(cov)
					lower = np.percentile(cov, 25)
					upper = np.percentile(cov, 75)
					x = [i for i in cov if i >= lower and i <= upper]
					normed_cov.append(sum(x)/len(x))
				if sum(normed_cov) < 0.01: continue
				initGraph.node[contigID]['coverage'] = normed_cov
				cov_data.append(normed_cov)
				
			# perform cov and kmer clustering	
			coverage_graph = self.coverage_clustering(cov_data, contigIDs, options)
			kmer_graph = self.ZScore_clustering(options)
			
			nodes = coverage_graph.nodes()
			if not quiet:
				sys.stdout.write('      Adding nodes and properties...\n')
			# ADD NODES
			for node, node_details in coverage_graph.nodes(data = True):
				try: coverage = node_details['coverage']
				except: coverage = []
				try: phylo = self.phylo_tags[node]
				except: phylo = []
				seed_genes = []
				consensus_tax = []
				consensus_paths = []
				for orfID in phylo:
					paths = []
					max_score = 0
					max_identity = 0
					for bitscore, mapping_details, taxID, geneID, taxPath in phylo[orfID]:
						if geneID != 'NA' and geneID not in seed_genes: seed_genes.append(geneID)
						align_perc = mapping_details.align_perc
						identity = mapping_details.identity
						paths.append(taxPath[::-1])
						if bitscore > max_score:
							max_score = bitscore
							max_identity = identity
					consensus_paths.append([max_score, unweightedConsensusPath(paths)])
				consensus_tax = weightedConsensusPath(consensus_paths)
				initGraph.add_node(node, coverage = coverage, seed_genes = seed_genes, taxonomy = consensus_tax)
			
			if not quiet:
				sys.stdout.write('      Nodes added, now connecting them with unified edges...\n')
			
			# ADD EDGES
			nodes = initGraph.nodes()
			for i, nodeA in enumerate(nodes):
				for j, nodeB in enumerate(nodes):
					if i >= j: continue
					# get the coverage edge
					if nodeA not in coverage_graph[nodeB]:
						corr_coeff = 0
						norm_manhattan = 0
					else: 
						corr_coeff = coverage_graph.edge[nodeA][nodeB]['corr_coeff']
						norm_manhattan = coverage_graph.edge[nodeA][nodeB]['norm_manhattan']
					# get the kmer edge
					if nodeA not in kmer_graph[nodeB]:
						kmer_coeff = 0
					else:
						kmer_coeff = np.median(kmer_graph.edge[nodeA][nodeB]['kmer_coeff'])
					# unifying edges
					if corr_coeff == 0: continue
					initGraph.add_edge(nodeA, nodeB, corr = (corr_coeff, norm_manhattan, kmer_coeff)) 
						
			if not quiet:
				sys.stdout.write('      Integrated graph initiated, now preserving it.\n')
			# pickle initGraph
			cPickle.dump(initGraph, open(graph_pickle, 'wb'))
			if not quiet:
				sys.stdout.write('      Done. Initial integrated graph preserved at: %s.\n' % graph_pickle)
		
		if os.path.exists(core_pickle):
			if not quiet:
				sys.stdout.write('      Loading initial cores from %s\n' % core_pickle)
			initCores = cPickle.load(open(core_pickle, 'rb'))
			if not quiet:
				sys.stdout.write('      Done.\n')
		else:
			# FORGING INIT CORES
			if not quiet:
				sys.stdout.write('      Parsing integrated graph into initial cores...\n')	
			# parse init_graph
			# to find init cores, use unified edge as W = log2(#samples+1)*corr_coeff + kmer_coeff
			G = nx.Graph()
			G.add_nodes_from(initGraph.nodes())
			alpha = np.log2(len(self.samples))
			for nodeA, nodeB, edge_details in initGraph.edges(data = True):
				W = alpha*edge_details['corr'][0]+edge_details['corr'][2]
				G.add_edge(nodeA, nodeB, weight = W)
			# partition using community	algorithm by Thomas Aynaud using the Louvain heuristices
			partition_dict = best_partition(G)
			partitions = {}
			for node in partition_dict:
				partitionID = partition_dict[node]
				if partitionID not in partitions: partitions[partitionID] = []
				partitions[partitionID].append(node)
		
			# evaluation nodes/contigs in intra/inter partition links, length, etc
			nodeX = {}
			for node in initGraph.nodes():
				nodeX[node] = {'length':int(node.split('.')[1]), 
								'intra_edges':0, 'inter_edges':0}
				
			for nodeA, nodeB in initGraph.edges():
				partitionA = partition_dict[nodeA]
				partitionB = partition_dict[nodeB]
				if partitionA == partitionB:
					nodeX[nodeA]['intra_edges'] += 1
					nodeX[nodeB]['intra_edges'] += 1
				else:
					nodeX[nodeA]['inter_edges'] += 1
					nodeX[nodeB]['inter_edges'] += 1
			
			initCores = {}
			for partitionID in partitions:
				initCores[partitionID] = []	
				# rank nodes based on length and R = inter/intra
				X = []
				for node in partitions[partitionID]:
					try: R = float(nodeX[node]['inter_edges'])/(nodeX[node]['inter_edges']+nodeX[node]['intra_edges'])
					except: R = 0.
					X.append((node, nodeX[node]['length'], R))
				ranked_length = map(itemgetter(0,1), sorted(X, key = lambda a: a[1], reverse = True))
				ranked_link = map(itemgetter(0,2), sorted(X, key = lambda a: a[2]))
				node_ranks = {}
				prev_length = 0
				r = 0
				for node, L in ranked_length:
					if L != prev_length:
						r += 1
						prev_length = L
					node_ranks[node] = r
				r = 0
				prev_R = 0
				for node, R in ranked_link:
					if R != prev_R:
						r += 1
						prev_R = R
					node_ranks[node] += r
				ranked_nodes = map(itemgetter(0), sorted(node_ranks.iteritems(), key = lambda a: a[1]))
				# get the top 50% or top 100 nodes
				for index in range(max(int(0.5*len(ranked_nodes)), 100)):
					try: node = ranked_nodes[index]
					except: continue
					initCores[partitionID].append(node)
			
			if not quiet:
				sys.stdout.write('      Preserving initial cores...\n')
			cPickle.dump(initCores, open(core_pickle, 'wb'))
			if not quiet:
				sys.stdout.write('      Done.\n')
		
		return initCores, initGraph

	def getRepNodes(self, cores, initGraph):
		coreID = 0
		rep_nodes = {}
		# translate xtag kmers into node kmer zscores
		node_kmer = {}
		for xtag in self.kmer_zscores:
			contigID = xtag.split('|')[0]
			if contigID not in node_kmer: node_kmer[contigID] = []
			node_kmer[contigID].append(self.kmer_zscores[xtag])
		node_avgKmer = {}
		for contigID in node_kmer:
			avgKmer = np.mean(node_kmer[contigID], axis = 0)
			node_avgKmer[contigID] = avgKmer
			
		for prev_coreID in cores:
			coreID += 1
			cov_data = []
			kmer_data = []
			seed_genes = {}
			for node in cores[prev_coreID]:
				cov_data.append(initGraph.node[node]['coverage'])
				kmer_data.append(node_avgKmer[node])
				for gene in initGraph.node[node]['seed_genes']:
					if gene not in seed_genes: seed_genes[gene] = []
					seed_genes[gene].append(node)
			rep_cov = np.mean(cov_data, axis = 0)
			rep_kmer = np.mean(kmer_data, axis = 0)
			rep_nodes[coreID] = {'coverage': rep_cov, 'kmer': rep_kmer, 'seed_genes': seed_genes, 'nodes': cores[prev_coreID]}			
		return rep_nodes
	
	def getFreeNodes(cores, initGraph):
		assigned_nodes = {}
		for coreID in cores:
			for node in cores[coreID]: assigned_nodes[node] = 1
		unassigned_nodes = []
		for node in initGraph.nodes():
			if node not in assigned_nodes: unassigned_nodes.append(node)
		return unassigned_nodes
	
	def mergeCores(self, rep_nodes, initGraph, max_iter):
		coreIDs = rep_nodes.keys()
		mergingGraph = nx.Graph()
		mergingGraph.add_nodes_from(coreIDs)
		for core_index, coreA in enumerate(coreIDs):
			for coreB in coreIDs[core_index+1:]:
				covA = rep_nodes[coreA]['coverage']
				covB = rep_nodes[coreB]['coverage']
				core_dist = norm_manhattan(covA, covB)
				kmerA = rep_nodes[coreA]['kmer']
				kmerB = rep_nodes[coreB]['kmer']
				kmer_coeff = np.corrcoef(kmerA, kmerB)[0,1]
				seedA = rep_nodes[coreA]['seed_genes']
				seedB = rep_nodes[coreB]['seed_genes']
				seed_boost = evaluate_seed(seedA, seedB)
				combined_dist = core_dist + (1.-kmer_coeff) + (1.-seed_boost)
				if combined_dist > 0.3: continue
				mergingGraph.add_edge(coreA, coreB)
		
		# merging them into cores
		cores = {}	
		for coreID, components in enumerate(nx.connected_components(mergingGraph)):
			cores[coreID] = []
			for x in components:
				for node in rep_nodes[x]['nodes']: cores[coreID].append(node)
		# iteratively recruit contigs
		iter = 0
		while 1:
			iter += 1
			membership_dict = query_cores(cores, initGraph)
			new_cores = {}
			for coreID in cores:
				new_cores[coreID] = []
				for node in cores[coreID]:
					new_cores[coreID].append(node)
			for node in membership_dict:
				m = membership_dict[node]
				purity = float(m[0][1])/sum(map(itemgetter(1), m))
				if purity > 0.95:
					new_cores[m[0][0]].append(node)
			diff = compare_cores(cores, new_cores)
			cores = new_cores
			if diff < 1 or iter > max_iter: break
		return cores

	def refine_cores(self, initCores, initGraph, options):
		cores = initCores
		quiet = options.quiet
		finalCores_pickle =  options.outdir+'/finalCores.cPickle'
		min_core = options.min_core
		max_iter = options.max_iter
		
		
		if os.path.exists(finalCores_pickle):
			if not quiet:
				sys.stdout.write('      Loading refined cores from %s.\n' % finalCores_pickle)
			finalCores = cPickle.load(open(finalCores_pickle, 'rb'))
			if not quiet:
				sys.stdout.write('      Done.\n')	
		else:
			global_num_nodes = 0
			global_total_bp = 0
			for node in initGraph.nodes():
				global_num_nodes += 1
				global_total_bp += int(node.split('.')[1])
			if not quiet:
				sys.stdout.write('      Total number of contigs to cluster: %i, in %i bp.\n' % (global_num_nodes, global_total_bp))
		
			iter = 0
			while 1:
				iter += 1
				num_cores, total_bp, num_contig = stat_cores(cores)
				if not quiet:
					sys.stdout.write('      [iter %i] %i cores, with %i bp (%i contigs) clustered.\n' % (iter, num_cores, total_bp, num_contig))
				membership_dict = query_cores(cores, initGraph)
				new_cores = {}
				for coreID in cores:
					new_cores[coreID] = []
					for node in cores[coreID]:
						new_cores[coreID].append(node)
				for node in membership_dict:
					m = membership_dict[node]
					purity = float(m[0][1])/sum(map(itemgetter(1), m))
					if purity > 0.95:
						new_cores[m[0][0]].append(node)
		
				# filter cores with size < min_core
				curr_cores = {}
				for coreID in new_cores:
					acc_length = 0
					for node in cores[coreID]:
						acc_length += int(node.split('.')[1])
					if acc_length < min_core: continue
					curr_cores[coreID] = new_cores[coreID]
				
				# compare two cores
				diff = compare_cores(cores, curr_cores)
				cores = {}
				cores = curr_cores
				if diff < 1 or iter > max_iter: break	
		
			num_cores, total_bp, num_contig = stat_cores(cores)
			if not quiet:
				sys.stdout.write('      Iteration finished with %i bp (%i contigs) clustered in %i cores.\n' % (total_bp, num_contig, num_cores))
			
			
			if not quiet:
				sys.stdout.write('      Adjusting cores...\n')
			# evaluate cores, split/merge cores
			# step-1, simplify cores into representative cov, kmer, seed_genes
			if not quiet:
				sys.stdout.write('      Simplifying cores with representative values.\n')
			rep_nodes = self.getRepNodes(cores, initGraph)
			if not quiet:
				sys.stdout.write('      Done.\n')
			
			# step-2, merge cores if necessary			
			if not quiet:
				sys.stdout.write('      Merging fragmented cores if any...\n')
			cores = self.mergeCores(rep_nodes, initGraph, max_iter)
			num_cores, total_bp, num_contig = stat_cores(cores)
			if not quiet:
				sys.stdout.write('      Merging step finished with %i bp (%i contigs) clustered into %i cores.\n' % (total_bp, num_contig, num_cores))
			
			"""
			# step-3, split cores if necessary
			if not quiet:
				sys.stdout.write('      Splitting lumpy cores if any...\n')
			for coreID in cores:
				seed_genes = {}
				for node in cores[coreID]:
					for seed in initGraph.node[node]['seed_genes']:
						if seed not in seed_genes: seed_genes[seed] = 1
						else: seed_genes[seed] += 1
				print coreID
				print seed_genes
			
			num_cores, total_bp, num_contig = stat_cores(cores)
			if not quiet:
				sys.stdout.write('      Splitting step finished with %i bp (%i contigs) clustered in %i cores.\n' % (total_bp, num_contig, num_cores))
			return cores
			"""
			# step-4, use looser criteria (cov) to assign contigs that are not assigned
			if not quiet:
				sys.stdout.write('      Assigning residual contigs to cores...\n')
			membership_dict = query_cores(cores, initGraph)
			finalCores = {}
			for coreID in cores:
				finalCores[coreID] = []
				for node in cores[coreID]:
					finalCores[coreID].append(node)
			for node in membership_dict:
				m = membership_dict[node]
				purity = float(m[0][1])/sum(map(itemgetter(1), m))
				if purity > 0.7:
					finalCores[m[0][0]].append(node)
			num_cores, total_bp, num_contig = stat_cores(finalCores)
			if not quiet:
				sys.stdout.write('      In final cores, %i bp (%i contigs) clustered in %i cores.\n' % (total_bp, num_contig, num_cores))
			
			if not quiet:
				sys.stdout.write('      Now preserving the results into %s...\n' % finalCores_pickle)
			cPickle.dump(finalCores, open(finalCores_pickle, 'wb'))
			# reserve coverage into cPickle
			coverage_pickle = options.outdir + '/contig_coverage.cPickle'
			coverage_data = {}
			for node, node_details in initGraph.nodes(data = True):
				cov = node_details['coverage']
				contigID = self.internal_IDs[node]
				coverage_data[contigID] = cov
			cPickle.dump(coverage_data, open(coverage_pickle, 'wb'))
			if not quiet:
				sys.stdout.write('      Done.\n')
		return finalCores
		
	def output_results(self, contig_bins, options):
		from Bio import SeqIO
		assembly = options.assembly
		quiet = options.quiet
		bindir = options.outdir+'/bins/'
		if not os.path.exists(bindir): os.mkdir(bindir)
		if not quiet:
			sys.stdout.write('      Outputting contigs into bins...\n')
		
		sequences = {}
		for record in SeqIO.parse(assembly, 'fasta'):
			sequences[record.description] = record.seq
		
		ufh = open(options.outdir + '/unassigned_contigs.fa', 'w')
		assigned_contigs = {}
		for binID in contig_bins:
			ofile = bindir + 'bin_'+str(binID+1)+'.fa'
			ofh = open(ofile, 'w')
			for node in contig_bins[binID]:
				contigID = self.internal_IDs[node]
				seq = sequences[contigID]
				ofh.write('>%s\n%s\n' % (contigID, seq))
				assigned_contigs[contigID] = 1
			ofh.close()
		
		for contigID in sequences:
			if contigID in assigned_contigs: continue
			ufh.write('>%s\n%s\n' % (contigID, sequences[contigID]))
		ufh.close()
		
		if not quiet:
			sys.stdout.write('      Done.\n')
			
		# get average coverage per bin
		if not quiet:
			sys.stdout.write('      Outputting average coverage of bins.\n')
		
		coverage_pickle = options.outdir + '/contig_coverage.cPickle'
		coverage_data = cPickle.load(open(coverage_pickle, 'rb'))
		bin_coverage_file = options.outdir + '/bin_coverage.txt'
		ofh = open(bin_coverage_file, 'w')
		ofh.write('#bin_ID\t%s\n' % ('\t'.join(self.samples)))
		for binID in contig_bins:
			cov_data = []
			for node in contig_bins[binID]:
				contigID = self.internal_IDs[node]
				cov_data.append(coverage_data[contigID])
			avgCov = np.mean(cov_data, axis = 0)
			cov_str = []
			for c in avgCov: cov_str.append('%.6f' % c)
			ofh.write('%s\t%s\n' % (str(binID+1), '\t'.join(cov_str)))
		ofh.close()
		if not quiet:
			sys.stdout.write('      Done.\n')
		return 0
					
	def print_proj(self):
		sys.stdout.write('Basic information of this BinGeR project:\n')
		sys.stdout.write('Samples:\n')
		for sample in self.samples:
			sys.stdout.write('  %s\n' % sample)
		sys.stdout.write('Read type: %s\n' % self.read_type)
		sys.stdout.write('K-mer size: %i\n' % self.kmer)
		sys.stdout.write('Minimum contig length: %i\n' % self.min_length)
		
################################## ITERATIVE CORE REFINEMENT ################################
def query_cores(cores, initGraph):
	# build cyflann index using initCores coverage
	core_dict = {}
	core_flags = []
	core_coverage = []
	for coreID in cores:
		for contig in cores[coreID]:
			core_dict[contig] = coreID
			cov = initGraph.node[contig]['coverage']
			if len(cov) == 0: continue
			core_coverage.append(cov)
			core_flags.append((coreID, contig))
	# build flann index
	cy = FLANNIndex(algorithm = 'kdtree')
	cy.build_index(core_coverage)
	
	# prepare query data
	query_data = []
	query_labels = []
	for node, node_details in initGraph.nodes(data = True):
		if node in core_dict: continue
		cov = node_details['coverage']
		if len(cov) == 0: continue
		query_data.append(cov)
		query_labels.append(node)
	
	# query in flann index
	membership_dict = {}
	cy_ids, cy_dists = cy.nn_index(query_data, min(len(core_dict), 100))
	for index, (results, dist) in enumerate(zip(cy_ids, cy_dists)):
		memberships = {}
		for seedID in results:
			coreID, contigID = core_flags[seedID]
			if coreID not in memberships: memberships[coreID] = 1
			else: memberships[coreID] += 1
		sorted_memberships = sorted(memberships.iteritems(), key = lambda x: x[1], reverse = True)
		membership_dict[query_labels[index]] = sorted_memberships
	return membership_dict

def stat_cores(cores):
	total_bp = 0
	num_contig = 0
	for coreID in cores:
		for node in cores[coreID]:
			num_contig += 1
			total_bp += int(node.split('.')[1])
	return len(cores), total_bp, num_contig

def compare_cores(prev_core, curr_core):
	diff = 0
	for coreID in prev_core:
		L1 = len(prev_core[coreID])
		try: L2 = len(curr_core[coreID])
		except: L2 = 0
		diff += abs(L2 - L1)
	return diff
		
################################## UTILITY FUNCTIONS ################################
def getCentroid(cov_dict, x):
	data = []
	for contigID in x: data.append(cov_dict[contigID])
	Y = distance.cdist(data, data, 'correlation')
	colDist = Y.sum(axis = 0)
	centroidIndex = np.argmin(colDist)
	return x[centroidIndex]

def evaluate_seed(seedA, seedB):
	comp = 0
	overlap = 0
	for seed in seedA:
		if seed in seedB: overlap += 1
		else: comp += 1
	for seed in seedB:
		if seed in seedA: overlap += 1
		else: comp += 1
	if comp+overlap == 0: return -1
	else: return float(comp)/(comp+overlap)
		
def count_seed_genes(G):
	X = {}
	for gene in SC_GENES: X[gene] = 0
	for node, node_details in G.nodes(data=True):
		for gene in node_details['seed_genes']: X[gene] += 1
	return X
	
def normed_seed_genes_counts(G, nodes, background_counts):
	counts = {}
	for node in nodes:
		seed_genes = G.node[node]['seed_genes']
		for gene in seed_genes:
			if gene not in counts: counts[gene] = 0
			counts[gene] += 1
	normed_counts = {}
	for gene in background_counts:
		b = background_counts[gene]
		try: c = counts[gene]
		except KeyError: c = 0
		if b == 0: normed_counts[gene] = -1
		else: normed_counts[gene] = float(c)/b
	return normed_counts

def evaluate_seed_genes(normed_counts):
	X = {}
	for gene in SC_GENES: X[gene] = 0
	for gene in seed_gene_counts: X[gene] = len(seed_gene_counts[gene])
	missing_count = 0
	redundant_count = 0
	for gene in X:
		if X[gene] == 0: missing_count += 1
		elif X[gene] > 1: redundant_count += 1
	return missing_count, redundant_count
	
def chunks(l, n):
	for i in xrange(0, len(l), n):
		yield l[i:i+n]

def norm_manhattan(X, Y):
	s = 0
	for a, b in zip(X, Y): s += abs(a-b)
	return s/min(X.sum(), Y.sum())

def unweightedConsensusPath(paths):
	consensus_path = []
	for x in zip(*paths):
		taxIDs = {}
		for taxID, name, rank in x: taxIDs[taxID] = 1
		if len(taxIDs.keys()) == 1: consensus_path.append(x[0])
	return consensus_path[::-1]

def weightedConsensusPath(consensus_paths):
	# construct a DiGraph
	X = nx.DiGraph()
	tax_dict = {}
	for score, path in consensus_paths:
		taxID_path = map(itemgetter(0), path)
		for taxID, name, rank in path: tax_dict[taxID] = (name, rank)
		X.add_path(taxID_path)
		for taxID in taxID_path:
			try: X.node[taxID]['score'] += score
			except: X.node[taxID]['score'] = score
		
	# get the thr = 0.9*overall_score
	thr = 0.9*sum(map(itemgetter(0), consensus_paths))
	# bottom-up approach to find LCA
	# get the root
	roots = [n for n, d in X.out_degree().items() if d == 0]
	# get the leaves
	leaves = [n for n,d in X.in_degree().items() if d==0]

	# get the highest scored consensus path
	candidate_paths = []
	for leaf in leaves:
		for root in roots:
			path = nx.shortest_path(X, leaf, root)
			for i, node in enumerate(path):
				if X.node[node]['score'] >= thr:
					P = []
					for nodeID in path[i:]:
						name, rank = tax_dict[nodeID]
						P.append((nodeID, name, rank))
					candidate_paths.append((X.node[node]['score'], P))
					break
					
	return candidate_paths

def print_options(options):
	sys.stdout.write('Input BinGeR project database: %s\n' % options.infile)
	sys.stdout.write('Directory for the output files: %s\n' % options.outdir)
	sys.stdout.write('Binning parameters:\n')
	sys.stdout.write('  Minimum core size: %.1e\n' % options.min_core)
	sys.stdout.write('  Maximum number of iterations: %.1e\n' % options.max_iter)
	sys.stdout.write('Runtime parameters:\n')
	sys.stdout.write('  Number of CPUs: %i\n\n' % options.num_proc)

################################### MAJOR FUNCTIONS #####################################
def BinGeR_binning(proj_data, options):
	quiet = options.quiet
	if not quiet:
		sys.stdout.write('Now start the binning process...\n')
		sys.stdout.write('Steps:\n  1. Initial graph and core construction\n')
		sys.stdout.write('  2. Interative cores refinement\n')
		sys.stdout.write('  3. Output bins\n\n')
	
	# init core
	if not quiet:
		sys.stdout.write('[1/3] Initializing integrated graph and cores ...\n')
	initCores, initGraph = proj_data.init_cores(options)
	
	# refining the core
	if not quiet:
		sys.stdout.write('[2/3] Iteratively refining cores...\n')
	contig_bins = proj_data.refine_cores(initCores, initGraph, options)
	
	# output the results
	if not quiet:
		sys.stdout.write('[3/3] Outputting bins...\n')
	proj_data.output_results(contig_bins, options)
	if not quiet:
		sys.stdout.write('Binning steps finished successfully!\n\n')
	return 0

# End of BinGeR_binning



################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + __version__)
	
	# Required arguments
	requiredOptions = OptionGroup(parser, "Required options",
								"These options are required to run BinGeR, and may be supplied in any order.")
	
	requiredOptions.add_option("-i", "--infile", type = "string", metavar = "FILE",
							help = "The BinGeR project database produced by BinGeR.preprocessing.py")
	
	requiredOptions.add_option("-a", "--assembly", type = "string", metavar = "FILE",
							help = "The co-assembly contigs in fasta format.")
	
	requiredOptions.add_option("-o", "--outdir", type = "string", metavar = "OUTDIR",
							help = "Working directory where the results and intermediate files will be stored at")

	parser.add_option_group(requiredOptions)

	# Binning parameters that could fine tune the process
	clusteringOptions = OptionGroup(parser, "Binning parameters",
						"There options are optional, and may be supplied in any order.")
	
	clusteringOptions.add_option("--min_core", type = "int", default = 1e5, metavar = 'INT',
							help = "Minimum size to consider as bin core [default: 1e5].")	
	
	clusteringOptions.add_option("--chunk_size", type = "int", default = 5000, metavar = 'INT',
							help = "Chunk size to perform Kmer and coverage clustering [default 5000].")	
	
	clusteringOptions.add_option("--max_iter", dest = "max_iter",
							type = "int", default = 100, metavar = 'INT',
							help = "Maximum number of iterations for core refinement [default: 100].")
						
	parser.add_option_group(clusteringOptions)

	# runtime settings that could affect the file saving and message printing
	runtimeSettings = OptionGroup(parser, "Runtime settings",
						"There options are optional, and may be supplied in any order.")
	
	runtimeSettings.add_option("-t", "--num_proc", type = "int", default = 1, metavar = 'INT',
							help = "Number of processor for BinGeR to use [default: 1].")
						
	runtimeSettings.add_option("-q", "--quiet", default = False, action = "store_true",
								help = "Suppress printing detailed runtime information, only important messages will show [default: False].")

	parser.add_option_group(runtimeSettings)

	(options, args) = parser.parse_args(argv)
	
	if options.infile is None:
		parser.error("[FATAL] A BinGeR project database produced by BinGeR.preprocessing.py is required!")
		exit(1)
	
	if options.outdir is None:
		parser.error("[FATAL] An output directory is required!")
		exit(1)
		
	if options.num_proc < 1:
		parser.error("Number of processors must be integer >= 1, you supplied %i" % options.num_proc)
		exit(1)
		
	if options.min_core < 1e4 or options.min_core > 1e6:
		parser.error("Size of minimum bin size must be in range [1e4, 1e6]bp, you supplied %i" % options.min_core)
		exit(1)
		
	if options.max_iter < 20 or options.max_iter > 200:
		parser.error("Maximum number of iterations for core refinement should be in in range [20, 200], you supplied %i" % options.max_iter)
		exit(1)
	
	
	# kickstart
	total_start_time = time()
	sys.stdout.write("BinGeR started at %s\n\n"%(ctime()))
	sys.stdout.flush()
	
	# print out basic project information
	print_options(options)
	
	# load BinGeR project database 
	try:
		if not options.quiet:
			sys.stdout.write('Loading BinGeR project database from: %s\n' % options.infile)
		proj_data = cPickle.load(open(options.infile, 'rb'))
		if not options.quiet:
			sys.stdout.write('Done.\n\n')
	except:
		sys.stderr.write('[FATAL] Error in loading BinGeR project database: %s\n' % options.infile)
		exit(1)
	
	# print out basic info about the project
	proj_data.print_proj()
	
	total_finish_time = time()
	sys.stdout.write("BinGeR started at %s\n\n"%(ctime()))
	sys.stdout.flush()
	
	if not os.path.exists(options.outdir):
		try:
			os.mkdir(options.outdir)
		except:
			sys.stderr.write('[FATAL] Cannot create output directory: %s\n' % options.outdir)
			exit(1)
	
	# main binning process
	BinGeR_binning(proj_data, options)
	
	################## UNDER CONSTRUCTION ##################
	# output
	
	total_finish_time = time()
	sys.stdout.write("BinGeR finished at %s\n\n"%(ctime()))
	sys.stdout.flush()

	return 0
	
# End of main

if __name__ == '__main__':
	main()

