BinGeR 
version: 0.1.2
author: Chengwei Luo

**BinGeR: in-situ Genome recovery tool for series metagenomes**
==========================

BinGeR: in-situ Genome recovery tool for series metagenomes

BinGeR (Binner for Genome Recovery) is a platform for de novo genome recovery from series metagenomes. It integrates information from single copy gene content, contig coverage correlation, and contig oligo-nucleotide composition correlation, and contig alignments to bin genome fragments together. It is written in Python, therefore it should run on Windows, Mac OS, and Unix/Linux.

Reference: [manuscript in preparation]

Copyright: Chengwei Luo, Konstantinidis Lab, Georgia Institute of Technology, 2013

Contact: Chengwei Luo (Email: luo.chengwei@gatech.edu; cluo@broadinstitute.org)

[How to install]
==========================

use git:

    git clone https://bitbucket.org/luo-chengwei/binger

use hg:

    hg clone https://bitbucket.org/luo-chengwei/binger

You can also download the zip archive of it from bitbucket repository: 

https://bitbucket.org/luo-chengwei/binger


In terminal cd to the unpacked directory, and run:

    $ python setup.py install

You can alway supply --prefix to alter the installation directory:

    $ python setup.py install --prefix = user/defined/installation/directory

If you have everything ready, this should take care of the installation.

[Dependencies]
==========================

* Python (2.6+)

* BLAT v.35+

* Bowtie2

* Python libraries:

>NetworkX (1.7+) 

>Numpy (1.5.1+)

>Scipy (0.12.0+)

>BioPython (1.58+)

>cyflann

>fastcluster

Packages with older versions might work, but not tested. You can certainly manually install all of the packages, however, I recommend using Anaconda, which is a Python distribution for big data processing, it bundles many packages for data scientists. For more information, go to:

https://store.continuum.io/cshop/anaconda/

Anaconda includes all the dependencies to run BinGeR.

CyFLANN is not shipped with Anaconda, but you can install it as:

    conda install -c http://conda.binstar.org/dougal cyflann

To install BinGeR, in terminal cd to the unpacked directory, and run:

    $ python setup.py install

You can alway supply --prefix to alter the installation directory:

    $ python setup.py install --prefix = user/defined/installation/directory

If you have everything ready, this should take care of the installation.

Fastcluster is not shipped with Anaconda either, you can find fastcluster at: http://danifold.net/fastcluster.html

The easy way to install it would be:

    easy_install --user -U fastcluster
    
If this doesn't work for you, please refer to its official page for more assistance.

[How to start a BinGeR project]
==================================
Here is what you need to get started:

1. A text file listing the sample names, one per line;

2. Coassembly of samples in fasta format. When running BinGeR.preprocessing.py, you'll need to use -a/--assemblies\_dir to specify where they are;

4. Read files for each sample named in pattern "[sample].1.\*" and "[sample].2.\*" or interleaved as "sample.\*" all saved in one folder. When running BinGeR.preprocessing, specify it using '-r/--reads\_dir'.

Now you are ready to run BinGeR.preprocessing.py and subsequentially, BinGeR.py

[How to run BinGeR]
==========================
After running setup.py, you may want to either open a new tab in the terminal, or in current terminal tab:
    
    $ source ~/.profile
    
or:

    $ source ~/.bashrc

There are two scripts to call: BinGeR.preprocessing.py and BinGeR.py. The former will build the database for your BinGeR project, and the latter will take the output database from BinGeR.preprocessing.py as input and perform the binning.

You should first run BinGeR.preprocessing as:

    $ BinGeR.preprocessing.py -l <samples.txt> -a <assembly_dir> -r <reads_dir> [options]

This will generate a BinGeR project database (by default, it outputs "BinGeR.preprocessing.dat" to your current working directory; you can specify it by -o/--outfile option).

With the output from BinGeR.preprocessing.py, you can run BinGeR.py as:
    
    $ BinGeR.py -i <input_BinGeR_project_database> -o <output_dir> -a <assembly_dir> [options]

For more detailed settings, please type:
    
    $ BinGeR.py --help/-h

and 

    $ BinGeR.preprocessing.py --help/-h
	
[BinGeR.preprocessing.py detailed usage]
============================
  Basic usage:

    $ BinGeR.preprocessing.py -l <samples.txt> -a <assembly_dir> -r <reads_dir> [options]

  Advanced usage:

  Options:
  
    --version             show program's version number and exit
    -h, --help            show this help message and exit

  Required options:
  
  These options are required to run BinGeR.preprocessing, and may be supplied in any order.

    -l FILE, --sample_list=FILE
                        Text file containing all sample names, one per line
    -r DIR, --reads_dir=DIR
                        Directory where reads in fastq or fasta format are,
                        naming follows "sample.*.1.fq" and "sample.*.2.fq"
                        convention
                        (or "sample.*.1.fa" and "sample.*.2.fa" for fasta
                        files).
    -a FILE, --assembly=FILE
                        The co-assembly contigs in fasta format.
    -o FILE, --outfile=FILE
                        The output library storing all the preprocessing data.

  Optional parameters:
  
  These options are optional, and may be supplied in any order.

    --read_type=STRING  How reads are stored: [pe]: the i-th forward end is
                        stored at *.1.fq and its reverse mate is the i-th
                        entry in *.2.fq; [interleaved]:
                        assuming the i-th read is the forward end and the
                        (i+1)-th read is the mate; [se]: unpaired single end
                        reads. [Default: pe]
    --tempdir=DIR       If re-run a previously failed task, you can use this
                        option to point to previous temp dir if there were
                        ambiguities in
                        locating tempdir; this automatically sets --continue
                        option to be True.

  Preprocessing parameters:
  
  These options are optional, and may be supplied in any order.

    --min_length=INT    Minimum contig length to be considered in binning
                        [default: 500].
    -k INT, --kmer=INT  K-mer size for oligo-nucleotide frequency statistics
                        [default: K = 4; range: 3-8].

  Runtime settings:
  
  These options are optional, and may be supplied in any order.

    -t INT, --num_proc=INT
                        Number of processor for BinGeR to use [default: 1].
    -q, --quiet         Suppress printing detailed runtime information, only
                        important messages will show [default: False].
    --keep              Keeps the folder that the intermediary files are
                        stored at [default: False].
    --continue          Continue from last run if it failed in the process.
                        [default: False].

  3rd party application parameters:
  
  These options are optional, and may be supplied in any order. If these binaries are not found in PATH,
  you can explicitly specify each of their locations by "--blat", "--bowtie2", and "--bowtie2_build",
  you can also just specify the Bowtie2 package directory by using "--bowtie2_dir".

    --bowtie2_dir=DIR   Path to bowtie2 bin directory, specify if bowtie2 and
                        bowtie2-build not in env path.
    --bowtie2=STRING    Path to bowtie2, specify if bowtie2 not in env path.
                        [default: bowtie2]
    --bowtie2_build=STRING
                        Path to bowtie2-build, specify if bowtie2-build not in
                        env path. [default: bowtie2-build]
    --samtools=STRING   Path to SAMtools, specify if SAMtools not in env path.
                        [default: samtools]
    --blat=STRING       Path to BLAT, specify if BLAT not in env path.
                        [default: blat]
                        
                        
[BinGeR.py detailed usage]
============================
  Basic usage:
  
    $ BinGeR.py -i <input_BinGeR_project_database> -o <output_dir> -a <assembly_dir> [options]
    
  Advanced usage:
  
  Options:
  
    --version             show program's version number and exit
    -h, --help            show this help message and exit

  Required options:
    
  These options are required to run BinGeR, and may be supplied in any order.

    -i FILE, --infile=FILE
                        The BinGeR project database produced by
                        BinGeR.preprocessing.py
    -a FILE, --assembly=FILE
                        The co-assembly contigs in fasta format.
    -o OUTDIR, --outdir=OUTDIR
                        Working directory where the results and intermediate
                        files will be stored at

  Binning parameters:

  There options are optional, and may be supplied in any order.

    --min_core=INT      Minimum size to consider as bin core [default: 1e5].
    --max_iter=INT      Maximum number of iterations for core refinement
                        [default: 100].

  Runtime settings:
    
  There options are optional, and may be supplied in any order.

    -t INT, --num_proc=INT
                        Number of processor for BinGeR to use [default: 1].
    -q, --quiet         Suppress printing detailed runtime information, only
                        important messages will show [default: False].

