#!/usr/bin/python
# -*- coding: utf-8 -*- 

"""
BinGeR (Binner for Genome Recovery): 
	in-situ Genome recovery tool for metagenomes

Copyright(c) 2014 Chengwei Luo (luo.chengwei@gatech.edu)


	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/BinGeR

for help, type:
python BinGeR.py --help
"""

import sys
import os
import glob
import cPickle
import shutil
import resource
from operator import itemgetter
from collections import deque
from Bio import SeqIO

NOFILE_LIMIT, NOVFILE_LIMIT = resource.getrlimit(resource.RLIMIT_NOFILE)

def outputBins(projInfo, options):
	binContigPath = projInfo.outdir + '/bins'
	if not os.path.exists(binContigPath):
		os.mkdir(binContigPath)

	bin_pickle = projInfo.outdir + '/Bins.cpickle'
	if not os.path.exists(bin_pickle):
		sys.stderr.write('FATAL: failure in locating the final bins serialized results.\n')
		exit(1)
	cpfh = open(bin_pickle, 'rb')
	bins = cPickle.load(cpfh)
	cpfh.close()
	
	# load contig membership
	contigIDs = {}
	for bin_index in bins:
		for contig in bins[bin_index]:
			contigIDs[contig] = bin_index
	
	# load map cPickle
	mapping_pickle = projInfo.outdir + '/preprocessing/internal_IDs.cPickle'
	if not os.path.exists(mapping_pickle):
		sys.stderr.write('FATAL: failure in locating internal ID mapping files.\n')
	map_data = cPickle.load(open(mapping_pickle, 'rb'))
	contig_map = {}
	for internal_ID in map_data:
		external_ID = map_data[internal_ID]
		if internal_ID in contigIDs: bin_membership = contigIDs[internal_ID]
		else: bin_membership = None
		contig_map[external_ID] = (internal_ID, bin_membership)
	
	# file handle writing
	coassembly = projInfo.outdir + '/preprocessing/coassembly.fa'
	ext_coassembly = options.coassembly
	ofhs = []
	numFileHandles = len(bins.keys())
	if numFileHandles < NOFILE_LIMIT-2:  # straightforward implementation
		ufh = open(binContigPath + '/' + sample + '.unclassified.contigs.fa', 'w')
		for binID in bins.keys():
			ofhs.append(open(binContigPath + '/'+ str(binID+1) + '/' + sample + '.contigs.fa','w'))
		for record in SeqIO.parse(ext_coassembly, 'fasta'):
			internal_ID, bin_membership = contig_map[record.description]
			seq = str(record.seq)
			if bin_membership == None:
				ufh.write('>%s\n%s\n' % (record.description, seq))
				continue
			ofh = ofhs[bin_membership]
			ofh.write('>%s\n%s\n' % (record.description, seq))
		for ofh in ofhs: ofh.close()
		ufh.close()
	else: # using dynamic file handles instead
		unclassifiedContigs = binContigPath + '/' + sample + '.unclassified.contigs.fa'
		ufh = open(unclassifiedContigs, 'w')
		activeFileHandles = 0
		for i in range(numFileHandles):
			if activeFileHandles < NOFILE_LIMIT - 3:
				binContigFile = binContigPath + '/'+ str(i+1) + '/' + sample + '.contigs.fa'
				ofh.append(open(binContigFile, 'a'))
			else:
				ofhs.append(None)
		# go through the coassembly and bin the contigs according to their membership	
		for record in SeqIO.parse(ext_coassembly, 'fasta'):
			internal_ID, bin_membership = contig_map[record.description]
			binID = bin_membership
			seq = str(record.seq)
			if bin_membership == None:
				ufh.write('>%s\n%s\n' % (record.description, seq))
				continue
			ofhIndex = bin_membership
			if ofhs[ofhIndex] == None:
				binContigFile = binContigPath + '/'+ str(binID+1) + '/' + sample + '.contigs.fa'
				# choose an active one to close
				fhSuc = 0
				for index, ofh in enumerate(ofhs[ofhIndex+1:]):
					if ofh != None:
						ofhs[index + ofhIndex + 1].close()
						ofhs[index + ofhIndex + 1] = None
						fhSuc = 1
						break
				if fhSuc == 0:
					for index, ofh in enumerate(ofhs[:ofhIndex]):
						if ofh != None:
							ofhs[index].close()
							ofhs[index] = None
							fhSuc = 1
							break
				# and then open the one we need
				ofhs[ofhIndex] = open(binContigFile, 'a')
		
			ofhs[ofhIndex].write('>%s\n%s\n'%(record.description, record.seq))
		ufh.close()
	
	# cleanup empty files
	for binID in binIDs:
		for file in glob.glob(binContigPath + '/' + str(binID+1) + '/*.fa'):
			if os.stat(file).st_size == 0:
				os.remove(file)
	
	# close up all filehandles
	for ofh in ofhs:
		if ofh == None:
			continue	
		ofh.close()
	
	sys.stdout.flush()
	if not options.quiet:
		sys.stdout.write('  All done. Contigs stored at:\n  %s\n' % binContigPath)
	
# End of outputBins

def reportStats(projInfo, options):
	logfile = projInfo.outdir + '/BinGeR.log'
	logfh = open(logfile, 'w')
	logfh.write('*********************** BinGeR ************************\n')
	logfh.write('[SETTINGS]\n')
	logfh.write('Samples:\n')
	for s in projInfo.samples:
		logfh.write('  %s\n' % s)
	logfh.write('Output directory: %s\n' % projInfo.outdir)
	logfh.write('Assemblies used from: %s\n' % projInfo.assemblies_dir)
	logfh.write('Reads used from: %s\n' % projInfo.reads_dir)
	logfh.write('Reads type: %s\n' % options.read_type)
	logfh.write('Number of threads: %i\n' % options.num_proc)
	logfh.write('K-mer, K = %i\n' % options.kmer)
	logfh.write('Minimum core size: %i\n' % options.min_core)
	logfh.write('Minimum number of reads to form linkage: %i\n' % options.min_linkage)
	logfh.write('Minimum contig lenght for coverage clustering: %i\n' % options.minCovLength)
	logfh.write('Minimum contig lenght for K-mer clustering: %i\n' % options.minKmerLength)
	logfh.write('Coverage clustering cutoff: %.3f\n' % options.minCovCorrcoef)
	logfh.write('K-mer clustering cutoff: %.3f\n' % options.minKmerCorrcoef)
	logfh.write('Personalized Community PageRank settings:\n')
	logfh.write('Dampening factor, alpha: %.3f\n' %options.cpr_alpha)
	logfh.write('Error tolerance, tol: %.3f\n' % options.cpr_tol)
	logfh.write('Maximum iterations: %i\n' % options.cpr_maxiter)
	logfh.close()
	
#	logfh.write('[OUTPUT BINS]\n')
#	logfh.write('bin_ID\tMax_size\tnumber_of_contigs\tG+C%\tInferred taxonomy\n')
	
# End of reportStats
