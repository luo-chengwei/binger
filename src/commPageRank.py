#!/usr/bin/python
# -*- coding: utf-8 -*- 

"""
BinGeR (Binner for Genome Recovery): 
	in-situ Genome recovery tool for metagenomes

Copyright(c) 2014 Chengwei Luo (luo.chengwei@gatech.edu)

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/BinGeR

for help, type:
python BinGeR.py --help
"""

import sys
import re
import random
import collections
import networkx as nx
from operator import itemgetter
import community

def commPageRank(G, seed_nodes, options):
	alpha = options.cpr_alpha
	tol = options.cpr_tol
	maxiter = options.cpr_maxiter
	#seeds = random.sample(G.nodes(), min(100, len(G)))
	sets = pprc([G, seed_nodes, alpha, tol, maxiter])
	return sets
	
# end of commPageRank

def pprc(args):
	"""
	This personalized PageRank clustering algorithm was originally designed by
	David F. Gleich at Purdue University. Here I tweaked it to suit the networkx 
	module and the weighted edge scenario with multiple seeds option.
	"""
	
	(G, seeds, alpha, tol, maxiter) = args

	Gvol = 2 * len(G.edges())
	
	# initialize personalization with seeds
	personalizationDict = {}
	for node in G.nodes():
		if node in seeds:
			personalizationDict[node] = 1
		else:
			personalizationDict[node] = 0
	
	try:		
		pr = nx.pagerank(G, alpha = alpha, max_iter = maxiter, 
				personalization = personalizationDict, tol = tol)
	
	except nx.exception.NetworkXError:	
		pr = {}
		r = {}
		Q = collections.deque()
		for s in seeds:
			r[s] = 1/len(seeds)
			Q.append(s)
			
		iterNum = 0
		while len(Q) > 0 and iterNum <= maxiter:
			v = Q.popleft()
			if v not in pr:
				pr[v] = 0
			pr[v] += (1-alpha)*r[v]
			mass = alpha*r[v]/(2*len(G[v]))
			for u in G[v]:
				if u not in r:
					r[u] = 0.
				if r[u] < len(G[u])*tol and r[u] + mass >= len(G[u])*tol:
					Q.append(u)
				r[u] = r[u] + mass
			r[v] = mass*len(G[v])
			if r[v] >= len(G[v])*tol:
				Q.append(v)
			iterNum += 1
		
	# sys.stdout.write('Finished init node values.\n')
	
	# find cluster 
	# normalized by weighted degree
	for v in pr:
		pr[v] = pr[v]/G.degree(v, weight = 'weight')
		
	# sort x's keys by value in decreasing order
	sv = sorted(pr.iteritems(), key = lambda x: x[1], reverse = True)
	S = set()
	volS = 0.
	cutS = 0.
	bestcond = 1.
	bestset = sv[0]
	for p in sv:
		s = p[0]  # get the vertex
		volS += G.degree(s, weight = 'weight')
		for v in G[s]:
			if v in S:
				cutS -= 1
			else:
				cutS += 1
		S.add(s)
		
		if Gvol == volS: continue
		
		if cutS/min(volS, Gvol - volS) < bestcond:
			bestcond = cutS/min(volS, Gvol - volS)
			bestset = set(S)
	
	return bestset
	
# End of pprc