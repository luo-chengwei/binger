#!/usr/bin/python
# -*- coding: utf-8 -*- 

__author__ = 'Chengwei Luo (luo.chengwei@gatech.edu)'
__version__ = '0.1.2'
__date__ = 'July 2014'

"""
BinGeR (Binner for Genome Recovery): 
	in-situ Genome recovery tool for metagenomes

Copyright(c) 2014 Chengwei Luo (luo.chengwei@gatech.edu)

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/BinGeR

for help, type:
python BinGeR.py --help
"""

from distutils.core import setup

setup(
	name = 'BinGeR',
	
	description = 'BinGeR (Binner for Genome Recovery): \
			in-situ Genome recovery tool for metagenomes',
	
	scripts =  ['BinGeR.py', 'BinGeR.preprocessing.py', 'src/commPageRank.py', 'src/community.py'],
	
	data_files = [('bin/db/', ['db/phylo.tar.gz','db/taxonomy.tar.gz'])],
	
	author=__author__,
    author_email="luo.chengwei@gatech.edu",
    version=__version__,
    url = "https://bitbucket.org/luo-chengwei/binger/",
    license="GNU GPL v3.0",
)